import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { scale } from 'utils';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SkeletonRow = (props) => {
  const {
    count,
    width,
    height,
    borderRadius,
    backgroundColor,
    highlightColor,
    marginRight,
  } = props;
  return (
    <View style={{ flexDirection: 'row' }}>
      {Array.from({ length: count }).map((item, index) => (
        <View key={index} style={{ marginRight: marginRight }}>
          <SkeletonPlaceholder
            backgroundColor={backgroundColor}
            highlightColor={highlightColor}>
            <View
              style={{
                height: height,
                width: width,
                borderRadius: borderRadius,
              }}></View>
          </SkeletonPlaceholder>
        </View>
      ))}
    </View>
  );
};

SkeletonRow.propTypes = {
  count: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  borderRadis: PropTypes.number,
  marginRight: PropTypes.number,
  backgroundColor: PropTypes.string,
  highlightColor: PropTypes.string,
};

SkeletonRow.defaultProps = {
  count: 1,
  width: scale(70),
  height: scale(35),
  borderRadius: scale(5),
  backgroundColor: '#24242C',
  highlightColor: 'gray',
  marginRight: scale(10),
};

export default React.memo(SkeletonRow);
