import React from 'react';
import { View, Text } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { METRICS, scale } from 'utils';

const SkeletonShowcase = () => {
  const renderSkeletonShowcase = (type, width, height) => {
    return (
      <View
        style={{
          width,
        }}>
        <SkeletonPlaceholder
          backgroundColor={'#24242C'}
          highlightColor={'gray'}>
          <SkeletonPlaceholder.Item flexDirection="row">
            <SkeletonPlaceholder.Item flex={1} justifyContent={'space-between'}>
              {type === 'left' ? (
                <SkeletonPlaceholder.Item
                  width={width}
                  height={height}
                  borderBottomRightRadius={scale(15)}
                  borderTopRightRadius={scale(15)}
                />
              ) : type === 'right' ? (
                <SkeletonPlaceholder.Item
                  width={width}
                  height={height}
                  borderBottomLeftRadius={scale(15)}
                  borderTopLeftRadius={scale(15)}
                />
              ) : (
                <SkeletonPlaceholder.Item
                  width={width}
                  height={height}
                  borderRadius={scale(15)}
                />
              )}
            </SkeletonPlaceholder.Item>
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      </View>
    );
  };
  return (
    <View
      style={{
        alignItems: 'center',
        position: 'relative',
      }}>
      {renderSkeletonShowcase(
        'center',
        Math.round(METRICS.screen.width * 0.6),
        Math.round((METRICS.screen.width * 3) / 4),
      )}
      <View
        style={{
          position: 'absolute',
          left: 0,
          alignItems: 'center',
          top:
            (Math.round((METRICS.screen.width * 3) / 4) -
              Math.round((METRICS.screen.width * 2.5) / 4)) /
            2,
        }}>
        {renderSkeletonShowcase(
          'left',
          scale(70),
          Math.round((METRICS.screen.width * 2.5) / 4),
        )}
      </View>
      <View
        style={{
          position: 'absolute',
          right: 0,
          alignItems: 'center',
          top:
            (Math.round((METRICS.screen.width * 3) / 4) -
              Math.round((METRICS.screen.width * 2.5) / 4)) /
            2,
        }}>
        {renderSkeletonShowcase(
          'right',
          scale(70),
          Math.round((METRICS.screen.width * 2.5) / 4),
        )}
      </View>
    </View>
  );
};

export default React.memo(SkeletonShowcase);
