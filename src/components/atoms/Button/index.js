import {
  React,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'libraries';
import PropTypes from 'prop-types';
import styles from './style';

const Button = (props) => {
  const {
    title,
    onPress,
    isLoading,
    loadingColor,
    textStyle,
    buttonStyle,
  } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.button, buttonStyle]}>
        {isLoading ? (
          <ActivityIndicator size="small" color={loadingColor} />
        ) : (
          <Text style={[styles.text, textStyle]}>{title}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
  loadingColor: PropTypes.string,
  textStyle: PropTypes.object,
  buttonStyle: PropTypes.object,
};

Button.defaultProps = {
  title: '',
  onPress: () => {},
  isLoading: false,
  loadingColor: 'black',
  textStyle: {},
  buttonStyle: {},
};

export default React.memo(Button);
