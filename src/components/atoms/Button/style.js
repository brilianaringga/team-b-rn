import {scale, Color} from 'utils';

const styles = {
  container: {
    flex: 1,
  },
  button: {
    backgroundColor: Color.primaryColor,
    padding: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
    margin: scale(5),
    borderRadius: scale(50),
    marginTop:scale(10),
    // minWidth: 350,
    minHeight: scale(50),
  },
  text: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: 'white',
  },
};

export default styles;
