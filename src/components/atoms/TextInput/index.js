import {
    React,
    View,
    Text,
    TouchableOpacity,
    ActivityIndicator,
    TextInput as RNTextInput
  } from 'libraries';
  import PropTypes from 'prop-types';
  import styles from './style';
  
  const TextInput = (props) => {
    const {
      title,
      onPress,
      isLoading,
      loadingColor,
      textStyle,
      textInputStyle,
      ...others
    } = props;
    return (
      <RNTextInput placeholderTextColor={'white'}
        style={[styles.inputtext, textInputStyle]}
        {...others}
      />
    );
  };
  
  TextInput.propTypes = {
    title: PropTypes.string,
    onPress: PropTypes.func,
    isLoading: PropTypes.bool,
    loadingColor: PropTypes.string,
    textStyle: PropTypes.object,
    textInputStyle: PropTypes.object,
  };
  
  TextInput.defaultProps = {
    title: '',
    onPress: () => {},
    isLoading: false,
    loadingColor: 'black',
    textStyle: {},
    textInputStyle: {}
  };
  
  export default React.memo(TextInput);
  