import {Color, scale} from 'utils';

const styles = {
  container: {
    flex: 1,
  },
  inputtext: {
    borderColor:Color.white,
    borderBottomWidth:scale(1),
    marginLeft:scale(50),
    marginRight:scale(50),
    marginTop:scale(5),
    fontSize:15,
    color: Color.white
  },
  text: {
    fontSize: scale(20),
    fontWeight: 'bold',
    color: Color.white,
  },
};

export default styles;