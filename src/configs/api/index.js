import ApiRequest from './config';
import baseUrl from './url';

const API = {};

//Example
API.getPhotos = ApiRequest.get(baseUrl.jsonPlaceholder.photos);

// Review
API.getMovieReviews = ApiRequest.get(baseUrl.reviews.movieReviews);
API.getUserReviews = ApiRequest.get(baseUrl.reviews.userReviews);
API.submitReview = ApiRequest.post(baseUrl.reviews.submit);
API.editReview = ApiRequest.put(baseUrl.reviews.edit);
API.deleteReview = ApiRequest.delete(baseUrl.reviews.delete);

//API Movies
API.getGenre = ApiRequest.get(baseUrl.movies.categoryList);
API.getMovieListByGenre = ApiRequest.get(baseUrl.movies.movieListByGenre);
API.getMovieTrending = ApiRequest.get(baseUrl.movies.trending);
API.getMoviePopular = ApiRequest.get(baseUrl.movies.popular);
API.getMovieNowPlaying = ApiRequest.get(baseUrl.movies.nowPlaying);
API.getMovieTopRated = ApiRequest.get(baseUrl.movies.topRated);
API.getMovieUpcoming = ApiRequest.get(baseUrl.movies.upcoming);
API.getMovieDetail = ApiRequest.get(baseUrl.movies.movieDetail);
API.searchMovies = ApiRequest.get(baseUrl.movies.searchMovies);
API.getSimilarMovies = ApiRequest.get(baseUrl.movies.similarMovies);

API.register = ApiRequest.post(baseUrl.profil.register);

API.Login = ApiRequest.post(baseUrl.profil.login);
API.Profile = ApiRequest.post(baseUrl.profil.profile_picture);
API.GetProfile = ApiRequest.post(baseUrl.profil.get_profile);
API.UpdateProfile = ApiRequest.post(baseUrl.profil.update_profile);
export default API;
