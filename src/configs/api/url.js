const base_url_placeholder = 'http://jsonplaceholder.typicode.com';
// const base_url_review_all = 'https://api.themoviedb.org';
const base_url_movies = 'http://139.59.124.53:3003';
const base_url_review = 'http://139.59.124.53:3003';

const baseUrl = {
  jsonPlaceholder: {
    photos: `${base_url_placeholder}/photos`,
  },
  reviews: {
    movieReviews: `${base_url_review}/api/review/movie/list`,
    userReviews: `${base_url_review}/api/review/user/list`,
    submit: `${base_url_review}/api/review/create`,
    edit: `${base_url_review}/api/review/movie/edit`,
    delete: `${base_url_review}/api/review/movie/delete`,
  },
  movies: {
    categoryList: `${base_url_movies}/api/movies/genre`,
    movieListByGenre: `${base_url_movies}/api/movies/list/genre`,
    trending: `${base_url_movies}/api/movies/list/trending`,
    topRated: `${base_url_movies}/api/movies/list/toprated`,
    upcoming: `${base_url_movies}/api/movies/list/upcoming`,
    nowPlaying: `${base_url_movies}/api/movies/list/nowplaying`,
    popular: `${base_url_movies}/api/movies/list/popular`,
    movieDetail: `${base_url_movies}/api/movies/detail-movie`,
    searchMovies: `${base_url_movies}/api/movies/search-movie`,
    similarMovies: `${base_url_movies}/api/movies/similar-movie`,
  },
  profil: {
    profile_picture: `${base_url_movies}/api/user/profile/picture`,
    get_profile: `${base_url_movies}/api/user/getprofile`,
    update_profile: `${base_url_movies}/api/user/updateprofile`,
    login: `${base_url_movies}/api/user/login`,
    register: `${base_url_movies}/api/user/signup`,
  },
};

export default baseUrl;
