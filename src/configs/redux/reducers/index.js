import { combineReducers } from 'redux';

import authReducer from './auth/authReducer';
import exampleReducer from './example/exampleReducer';
import moviesReducer from './movies/moviesReducer';
import reviewsReducer from './reviews/reviewsReducer';
import profileReducer from './profile/profileReducer';

const reducers = {
  reviewStore: reviewsReducer,
  exampleStore: exampleReducer,
  movieStore: moviesReducer,
  authStore: authReducer,
  profileStore: profileReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
