const defaultState = {
    isLoading: false,
    fullname: '',
    username: '',
    email:'',
    error:null,
    errorRegister: null,
    isLoadingRegister: false,
     };
  
 

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'SESSION_EXPIRED': {
      return defaultState;
    }
    case 'LOGIN_REQUEST': {
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    }
    case 'REGISTER_REQUEST': {
      return {
        ...state,
        isLoadingRegister: true,
        errorRegister: null,
      };
    }
    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        fullname: action.payload.fullname,
        username: action.payload.username,
        email: action.payload.email,
        isLoading: false,
        error: false,
      };
    }
    case 'REGISTER_SUCCESS': {
      return {
        ...state,
        isLoadingRegister: false,
        errorRegister: false,
      };
    }
    case 'LOGIN_FAILED': {
      return {
        ...defaultState,
        error: action.payload.error,
      };
    }
    case 'REGISTER_FAILED': {
      return {
        ...defaultState,
        errorRegister: action.payload.error,
      };
    }
    default:
      return state;
  }
};
