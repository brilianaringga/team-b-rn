import API from 'configs/api';
import * as RootNavigation from 'configs/routes/RootNavigation';
import { AsyncStorage, showMessage } from 'libraries';

export function expiredAction() {
  return async (dispatch) => {
    showMessage({
      icon: 'auto',
      message: 'Sorry',
      description: 'Your Session has been expired',
      type: 'warning',
    });
    RootNavigation.reset('LoginPage');
    dispatch({
      type: 'SESSION_EXPIRED',
    });
  };
}

export const LoginAction = (payload) => {
  //console.log('test')
    return async (dispatch) => {
        // POST LOGIN REQUEST
        dispatch({
            type: 'LOGIN_REQUEST',  
          });
         
      try {
        const res = await API.Login(payload)
        //console.log('error')
        //console.log(res.status)
        // CATCH POST LOGIN SUCCESS
        if (res) {
          await AsyncStorage.setItem('@token', res.data.token)
          dispatch({
            type: 'LOGIN_SUCCESS',
            payload: {
                fullname:res.data.fullname,
                username:res.data.username,
                 email: res.data.email,
                 error: false, 
        },
      });
      RootNavigation.reset('BottomTab');
    }  
      
      } catch (err) {
        //console.log (err)
        // CATCH POST LOGIN ERROR
        dispatch({
          type: 'LOGIN_SUCCESS',
          payload: {
            fullname: res.data.fullname,
            username: res.data.username,
            email: res.data.email,
            error: false,
          },
        });
        RootNavigation.reset('BottomTab');
      }
    } 
  };


export function exampleAction(data) {
  return async (dispatch) => {
    //contoh dispaatch update data
    dispatch({
      type: 'UPDATE_DATA',
      payload: 'ini data baru',
    });

    //contoh dispaatch update email
    dispatch({
      type: 'UPDATE_EMAIL',
      payload: 'indra.cip@gmail.com',
    });
  };
}

export function getDataApi(start, limit) {
  return async (dispatch) => {
    try {
      //---- untuk case api dengan method Get, yang ad parameter dan headers, bisa di taro  kyk gini ----
      // const payload = {
      //   params: {
      //     _start: start,
      //     _limit: limit
      //   },
      //   headers: {
      //     Authorization: "Bearer ejydcsdhcvdscsbdcvvscvdsvcvdsgcvsdghcv"
      //   }
      // }

      //---- untuk case api dengan method Post, Put, atau Delete yang butuh kirim body dan header,bisa di taro di dalam body and headers kyk gini ----
      // const payload = {
      //   body: {
      //     username: "username",
      //     password: "password"
      //   },
      //   headers: {
      //     Authorization: "Bearer ejydcsdhcvdscsbdcvvscvdsvcvdsgcvsdghcv"
      //   }
      // }

      const payload = {
        params: {
          _start: start,
          _limit: limit,
        },
      };
      const data = await API.getPhotos(payload);
      if (data) {
        dispatch({
          type: 'FETCH_DATA',
          payload: data,
        });
        // return Promise.resolve(data);
      }
      // return Promise.reject();
    } catch (e) {
      console.log('getDataApi Err: \n', e);
      // return Promise.reject(e);
    }
  };
}

export function checkTokenValidation() {
  return async (dispatch) => {
    try {
      const res = await API.GetProfile();
      if(res) {
        RootNavigation.reset('BottomTab');
      }
      console.log('res: ' + res);
    }
    catch(e) {
      if(e.status == 401) {
        dispatch(expiredAction());
      }
      console.log(e.status);
    }
  };
}

export const RegisterAction = (payload) => {
  console.log('ini payload', payload);
  return async (dispatch) => {
    // POST LOGIN REQUEST
    dispatch({
      type: 'REGISTER_REQUEST',
    });

    try {
      console.log(payload);
      const res = await API.register(payload);
      //console.log('error')
      console.log(res);
      // CATCH POST LOGIN SUCCESS
      if (res) {
        console.log(res)
        // if 
        // console.log('ini token', res.data.token);
        // // await AsyncStorage.setItem('@token', res.data.token);
        dispatch({
          type: 'REGISTER_SUCCESS',
          // payload: {
          //   fullname: res.data.fullname,
          //   username: res.data.username,
          //   email: res.data.email,
          //   error: false,
          // },
        });
        RootNavigation.reset('LoginPage');
      }
    } catch (err) {
      console.log('ini Error', err);
      // CATCH POST LOGIN ERROR
      dispatch({
        type: 'REGISTER_FAILED',
        payload: {
          error: err,
        },
      });
    }
  };
};
