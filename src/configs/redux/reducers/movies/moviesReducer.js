const defaultState = {
  data_trending: [],
  data_popular: [],
  data_topRate: [],
  data_movie_detail: {
    backdrop_path: '',
    title: '',
    runtime: '',
    vote_average: '',
    release_date: '',
    genres: [],
    overview: '',
    reviews: 0,
  },
  data_upcoming: [],
  data_genre: [],
  data_movie_genre: [],
  data_search: [],
  data_similar: [],
  isLoadingMovieDetail: false,
  isLoadingSearch: false,

  error: {
    genre: {},
    movieListGenre: {},
    trending: {},
    popular: {},
    topRate: {},
    upcoming: {},
    movieDetail: {},
    search: {},
    similar: {},
  },
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'UPDATE_DATA_GENRE': {
      return {
        ...state,
        data_genre: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_GENRE': {
      return {
        ...state,
        data_movie_genre: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_TRENDING': {
      return {
        ...state,
        data_trending: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_POPULAR': {
      return {
        ...state,
        data_popular: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_TOPRATE': {
      return {
        ...state,
        data_topRate: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_SIMILAR': {
      return {
        ...state,
        data_similar: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_SEARCH': {
      return {
        ...state,
        isLoadingSearch: false,
        data_search: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_DETAIL': {
      return {
        ...state,
        isLoadingMovieDetail: false,
        data_movie_detail: action.payload,
      };
    }
    case 'UPDATE_DATA_MOVIE_LIST_UPCOMING': {
      return {
        ...state,
        data_upcoming: action.payload,
      };
    }
    case 'GETTING_MOVIE_BY_GENRE': {
      return {
        ...state,
        error: {
          ...state.error,
          movieListGenre: {},
        },
      };
    }
    case 'GETTING_TRENDING_MOVIE': {
      return {
        ...state,
        error: {
          ...state.error,
          trending: {},
        },
      };
    }
    case 'GETTING_POPULAR_MOVIE': {
      return {
        ...state,
        error: {
          ...state.error,
          popular: {},
        },
      };
    }
    case 'GETTING_TOPRATE_MOVIE': {
      return {
        ...state,
        error: {
          ...state.error,
          topRate: {},
        },
      };
    }
    case 'GETTING_SIMILAR_MOVIE': {
      return {
        ...state,
        error: {
          ...state.error,
          similar: {},
        },
      };
    }
    case 'GETTING_SEARCH_MOVIE': {
      return {
        ...state,
        isLoadingSearch: true,
        error: {
          ...state.error,
          search: {},
        },
      };
    }
    case 'GETTING_MOVIE_DETAIL': {
      return {
        ...state,
        isLoadingMovieDetail: true,
        error: {
          ...state.error,
          movieDetail: {},
        },
      };
    }
    case 'GETTING_UPCOMING_MOVIE': {
      return {
        ...state,
        error: {
          ...state.error,
          upcoming: {},
        },
      };
    }
    case 'FETCH_GENRE_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          genre: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_GENRE_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          movieListGenre: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_TRENDING_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          trending: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_POPULAR_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          popular: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_TOPRATE_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          topRate: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_SIMILAR_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          similar: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_SEARCH_FAILED': {
      return {
        ...state,
        isLoadingSearch: false,
        error: {
          ...state.error,
          search: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_DETAIL_FAILED': {
      return {
        ...state,
        isLoadingMovieDetail: false,
        error: {
          ...state.error,
          movieDetail: action.payload,
        },
      };
    }
    case 'FETCH_MOVIE_LIST_UPCOMING_FAILED': {
      return {
        ...state,
        error: {
          ...state.error,
          upcoming: action.payload,
        },
      };
    }
    case 'FETCH_GENRE': {
      return {
        ...state,
        error: {
          ...state.error,
          genre: {},
        },
      };
    }
    default:
      return state;
  }
};
