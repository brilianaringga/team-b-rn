import API from 'configs/api';
import { expiredAction } from '../auth/authActions';

export function getGenreList() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'FETCH_GENRE',
      });
      const payload = {
        params: {
          lang: 'en-EN',
        },
      };
      const res = await API.getGenre(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_GENRE',
          payload: res.data.genres ? res.data.genres : [],
        });
      } else {
        dispatch({
          type: 'FETCH_GENRE_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      console.log('ini error', e.status);
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_GENRE_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getMovieListbyGenre(payload) {
  console.log(payload);
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_MOVIE_BY_GENRE',
      });
      payload.params.lang = 'en-EN';

      const res = await API.getMovieListByGenre(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_GENRE',
          payload: res.data.movies.results ? res.data.movies.results : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_GENRE_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_GENRE_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getTrendingMovie() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_TRENDING_MOVIE',
      });

      const res = await API.getMovieTrending();
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_TRENDING',
          payload: res.data.trending ? res.data.trending : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_POPULAR_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_POPULAR_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getPopularMovie() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_POPULAR_MOVIE',
      });

      const res = await API.getMoviePopular();
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_POPULAR',
          payload: res.data.movies.results ? res.data.movies.results : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_POPULAR_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_POPULAR_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getUpComingMovie() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_UPCOMING_MOVIE',
      });

      const res = await API.getMovieUpcoming();
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_UPCOMING',
          payload: res.data.movies.results ? res.data.movies.results : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_POPULAR_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_POPULAR_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getTopRateMovie() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_TOPRATE_MOVIE',
      });

      const res = await API.getMovieTopRated();
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_TOPRATE',
          payload: res.data.movies.results ? res.data.movies.results : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_TOPRATE_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_TOPRATE_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getMovieDetail(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_MOVIE_DETAIL',
      });

      const res = await API.getMovieDetail(payload);
      if (res) {
        const data = res.data ? res.data : {};
        data.reviews =
          res.reviews && res.reviews.length > 0
            ? res.reviews[0].count_reviews
            : 0;
        dispatch({
          type: 'UPDATE_DATA_MOVIE_DETAIL',
          payload: data,
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_DETAIL_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_DETAIL_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function searchMovies(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_SEARCH_MOVIE',
      });

      const res = await API.searchMovies(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_SEARCH',
          payload: res.data ? res.data : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_SEARCH_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_SEARCH_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getSimilarMovie(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_SIMILAR_MOVIE',
      });

      const res = await API.getSimilarMovies(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_DATA_MOVIE_LIST_SIMILAR',
          payload: res.data ? res.data : [],
        });
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_SIMILAR_FAILED',
          payload: 'no data',
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      } else {
        dispatch({
          type: 'FETCH_MOVIE_LIST_SIMILAR_FAILED',
          payload: e,
        });
      }
    }
  };
}
