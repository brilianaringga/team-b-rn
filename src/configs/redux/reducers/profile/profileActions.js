import API from 'configs/api';
import {AsyncStorage} from 'libraries'
/*
Example Action
*/

export const GetProfileAction = (payload) => {
    return async (dispatch) => {

            const res = await API.GetProfile(payload)
            // CATCH POST LOGIN SUCCESS
            if (res) {
              dispatch({
                type: 'GET_PROFILE',
                payload: {
                    fullname:res.data.fullname,
                    username:res.data.username,
                     email: res.data.email,    
            }})
                 
            }
        }
    }     

export const UpdateImageAction = (payload) => {
    return async (dispatch) => {
        //console.log('dispatch')
        //console.log(payload)
        const res = await API.Profile(payload)
            // CATCH POST UPDATE IMAGE SUCCESS
            if (res) {
              dispatch({
                type: 'UPDATE_IMAGE_PROFILE',
                payload: {
                profile_picture: res.data.profile_picture,    
            }})
                 
            }
       
    }
}

  export const UpdateProfileAction = (payload) => {
    return async (dispatch) => {

        const res = await API.UpdateProfile(payload)
            // CATCH POST UPDATE PROFIL SUCCESS
            if (res) {
              dispatch({
                type: 'UPDATE_PROFILE',
                payload: {
                    fullname:res.data.fullname,
                    username:res.data.username,
                     email: res.data.email,
                         
            }})
                 
            }
        
    }
}

export const OnChangeFullname = (payload) => {
    return async (dispatch) => {

              dispatch({
                type: 'ONCHANGE_USERNAME',
                payload: {
                    fullname:payload.fullname,
                    
                         
            }})
                 
            }
        
    }

    export const OnChangePassword = (payload) => {
        return async (dispatch) => {
    
                  dispatch({
                    type: 'ONCHANGE_PASSWORD',
                    payload: {
                        password:payload.password,
                        
                             
                }})
                     
                }
            
        }



  export const LogoutAction = (payload) => {
    return async (dispatch) => {

        await AsyncStorage.removeItem('@token')
        
    };
  };