const defaultState = {
    fullname: '',
    username: '',
    email:'',
    password:'',
    profile_picture:''
     };


     export default (state = defaultState, action = {}) => {
        switch (action.type) {

            case 'GET_PROFILE' :
                return {
                    ...state,
                    fullname: action.payload.fullname,
                    username: action.payload.username,
                    email: action.payload.email,
                  };

            case 'UPDATE_IMAGE_PROFILE' :
                return {
                    ...state,
                    profile_picture: action.payload.profile_picture,
                };
            
        
            case 'UPDATE_PROFILE' :
            return {
                ...state,
                fullname: action.payload.fullname,
                username: action.payload.username,
                email: action.payload.email,
                
              };

              case 'ONCHANGE_USERNAME' :
            return {
                ...state,
                fullname: action.payload.fullname,
                
              };

              case 'ONCHANGE_PASSWORD' :
            return {
                ...state,
                password: action.payload.password,
                
              };
            
          default:
            return state;
        }
      };
      