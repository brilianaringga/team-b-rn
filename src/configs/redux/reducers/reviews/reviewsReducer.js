import { ReviewActionType } from 'utils/constant';

const initialState = {
  data_movies_reviews: [],
  data_user_reviews: [],
  user_reviews_current_page: 1,
  user_reviews_current_totalpages: 1,
  user_reviews_current_totalresults: 1,
  error: {},
  isLoading: false,
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ReviewActionType.FETCH_MOVIE_REVIEWS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ReviewActionType.FETCH_MOVIE_REVIEWS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data_movies_reviews: action.payload,
      };
    case ReviewActionType.FETCH_MOVIE_REVIEWS_FAILED:
      return {
        ...state,
        isLoading: false,
        data_movies_reviews: action.payload,
        error: action.error,
      };

    case ReviewActionType.FETCH_USER_REVIEWS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ReviewActionType.FETCH_USER_REVIEWS_SUCCESS:
      console.log(action.payload.results);
      return {
        ...state,
        isLoading: false,
        data_user_reviews: action.payload.results,
        user_reviews_current_page: action.payload.page,
        user_reviews_current_totalpages: action.payload.totalPages,
        user_reviews_current_totalresults: action.payload.totalResults,
      };
    case ReviewActionType.FETCH_USER_REVIEWS_FAILED:
      return {
        ...state,
        isLoading: false,
        data_user_reviews: action.payload,
        user_reviews_current_page: 1,
        user_reviews_current_totalpages: 1,
        user_reviews_current_totalresults: 1,
        error: action.error,
      };

    case ReviewActionType.SUBMIT_REVIEW_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ReviewActionType.SUBMIT_REVIEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case ReviewActionType.SUBMIT_REVIEW_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    case ReviewActionType.EDIT_REVIEW_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ReviewActionType.EDIT_REVIEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case ReviewActionType.EDIT_REVIEW_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    case ReviewActionType.DELETE_REVIEW_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ReviewActionType.DELETE_REVIEW_SUCCESS:
      return {
        ...state,
        isLoading: false,
      };
    case ReviewActionType.DELETE_REVIEW_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    default:
      return state;
  }
};
