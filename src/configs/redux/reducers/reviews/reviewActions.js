import API from 'configs/api';
import { ReviewActionType } from 'utils/constant';

export const getMovieReviews = (data) => {
  console.log('======== GET MOVIE REVIEWS ========');
  return async (dispatch) => {
    try {
      dispatch({
        type: ReviewActionType.FETCH_MOVIE_REVIEWS_REQUEST,
      });

      const response = await API.getMovieReviews(data);
      console.log('======== MASUK SUCCESS ========');
      dispatch({
        type: ReviewActionType.FETCH_MOVIE_REVIEWS_SUCCESS,
        payload: response.data.reviews,
      });
    } catch (error) {
      console.log('======== MASUK ERROR ========');
      const errResp = error.data.error;
      if (errResp) {
        switch (errResp.name) {
          case 'empty-review':
            dispatch({
              type: ReviewActionType.FETCH_MOVIE_REVIEWS_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: ReviewActionType.FETCH_MOVIE_REVIEWS_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: ReviewActionType.FETCH_MOVIE_REVIEWS_FAILED,
          error: error,
        });
      }
    }
  };
};

export const getUserReviews = (data) => {
  console.log('======== GET USER REVIEWS ========');
  return async (dispatch) => {
    try {
      dispatch({
        type: ReviewActionType.FETCH_USER_REVIEWS_REQUEST,
      });

      const response = await API.getUserReviews(data);
      // console.log(response.data.reviews);
      console.log('======== INI RESPONSE NYA========');

      dispatch({
        type: ReviewActionType.FETCH_USER_REVIEWS_SUCCESS,
        payload: response.data.reviews,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (errResp) {
        switch (errResp.name) {
          case 'empty-review':
            dispatch({
              type: ReviewActionType.FETCH_USER_REVIEWS_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: ReviewActionType.FETCH_USER_REVIEWS_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: ReviewActionType.FETCH_USER_REVIEWS_FAILED,
          error: error,
        });
      }
    }
  };
};

export const submitReview = (data) => {
  console.log('======== SUBMIT REVIEW ========');
  return async (dispatch) => {
    try {
      dispatch({
        type: ReviewActionType.SUBMIT_REVIEW_REQUEST,
      });

      await API.submitReview(data);

      dispatch({
        type: ReviewActionType.SUBMIT_REVIEW_SUCCESS,
        // payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: ReviewActionType.SUBMIT_REVIEW_FAILED,
        error: error,
      });
      return { error: error };
    }
  };
};

export const editReview = (data) => {
  console.log('======== EDIT REVIEWS ========');
  return async (dispatch) => {
    try {
      dispatch({
        type: ReviewActionType.EDIT_REVIEW_REQUEST,
      });

      const response = await API.editReview(data);
      dispatch({
        type: ReviewActionType.EDIT_REVIEW_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      dispatch({
        type: ReviewActionType.EDIT_REVIEW_FAILED,
        error: error,
      });
      return { error: error };
    }
  };
};

export const deleteReview = (data) => {
  console.log('======== DELETE REVIEWS ========');
  return async (dispatch) => {
    try {
      dispatch({
        type: ReviewActionType.DELETE_REVIEW_REQUEST,
      });

      const response = await API.deleteReview(data);
      dispatch({
        type: ReviewActionType.DELETE_REVIEW_SUCCESS,
      });
    } catch (error) {
      dispatch({
        type: ReviewActionType.DELETE_REVIEW_FAILED,
        error: error,
      });
      return { error: error };
    }
  };
};
