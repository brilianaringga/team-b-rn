import {
  createStackNavigator,
  TransitionSpecs,
  TransitionPresets,
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {
  AllReviewPage,
  ExamplePage,
  LoginPage,
  MovieDetailsPage,
  MyReviewPage,
  ProfilePage,
  RegisterPage,
  SplashScreen,
  HomePage,
  SearchPage,
  ExampleLogin,
} from 'pages';

import {
  React,
  createBottomTabNavigator,
  FontAwesome5,
  View,
  BottomTabBar,
  Platform,
} from 'libraries';
import { Color, FONTS, scale } from 'utils';
import FabButton from 'components/svg/fab';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-eva-icons';
// import { isReadyRef, navigationRef } from 'react-navigation-helpers';
import { navigationRef, isReadyRef } from './RootNavigation';

const Stack = createStackNavigator();
const EmptyScreen = () => {
  return null;
};
/*--------------------------
     Transition Options
---------------------------
*/

const CustomTransition = {
  headerShown: false,
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec,
  },
  ...TransitionPresets.SlideFromRightIOS,
};

const BottomTab = createBottomTabNavigator();

/*--------------------------
     Bottom Navigation
---------------------------
*/

function BottomNavigation() {
  return (
    <BottomTab.Navigator
      initialRouteName="HomePage"
      tabBar={(props) => (
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,
            backgroundColor: Color.black,
          }}>
          <BottomTabBar {...props} />
        </View>
      )}
      tabBarOptions={{
        activeTintColor: Color.primaryColor,
        activeBackgroundColor: Color.primaryColor,
        showLabel: false,
        labelStyle: {
          fontFamily: FONTS.quickSand,
        },
        style: {
          borderTopWidth: 0,
          backgroundColor: 'transparent',
          elevation: 30,
        },
        tabStyle: {
          backgroundColor: Color.black,
          // marginBottom: Platform.OS === 'ios' ? scale(-10) : 0,
        },
      }}>
      <BottomTab.Screen
        name="HomePage"
        component={HomePage}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: (tabInfo) => (
            <Icon name="home" width={24} height={24} fill={tabInfo.color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="SearchPage"
        component={SearchPage}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: (tabInfo) => (
            <Icon name="search" width={24} height={24} fill={tabInfo.color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="MyReviewPage"
        component={MyReviewPage}
        options={{
          unmountOnBlur: true,
          tabBarLabel: 'My Review',
          tabBarIcon: (tabInfo) => (
            // <FontAwesome5 name={'tasks'} color={tabInfo.color} size={20} />
            <Icon
              name="message-circle-outline"
              width={24}
              height={24}
              fill={tabInfo.color}
            />
          ),
        }}
      />

      <BottomTab.Screen
        name="ProfilePage"
        component={ProfilePage}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: (tabInfo) => (
            <Icon
              name="person-outline"
              width={24}
              height={24}
              fill={tabInfo.color}
            />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

/*--------------------------
     Routes
---------------------------
*/

class Routes extends React.Component {
  componentWillUnmount() {
    isReadyRef.current = false;
  }
  render() {
    return (
      <NavigationContainer
        ref={navigationRef}
        onReady={() => {
          isReadyRef.current = true;
        }}>
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={CustomTransition}>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="RegisterPage" component={RegisterPage} />
          <Stack.Screen name="LoginPage" component={LoginPage} />
          <Stack.Screen name="ExamplePage" component={ExamplePage} />
          <Stack.Screen name="MovieDetailsPage" component={MovieDetailsPage} />
          <Stack.Screen name="AllReviewPage" component={AllReviewPage} />
          <Stack.Screen name="BottomTab" component={BottomNavigation} />
          <Stack.Screen name="ProfilPage" component={ProfilePage} />
          <Stack.Screen name="ExampleLogin" component={ExampleLogin} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Routes;
