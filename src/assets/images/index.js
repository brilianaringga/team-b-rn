const deliveryMan = require('./deliveryMan.png');
const fruits1 = require('./fruits1.png');
const fruits2 = require('./fruits2.png');
const fruitsSmall = require('./fruitsSmall.png');
const logo = require('./logo.png');
const map = require('./map.png');
const orderPrepare = require('./orderPrepare.png');
const orderTaken = require('./orderTaken.png');
const background = require('./background.jpg')

const IMG = {
  deliveryMan,
  fruits1,
  fruits2,
  fruitsSmall,
  logo,
  map,
  orderPrepare,
  orderTaken,
  background,
};
export default IMG;
