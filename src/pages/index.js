import AllReviewPage from './allReviewPage';
import ExamplePage from './examplePage';
import LoginPage from './login';
import MovieDetailsPage from './movieDetails';
import MyReviewPage from './myReviewPage';
import ProfilePage from './profile';
import RegisterPage from './register';
import SplashScreen from './splash';
import HomePage from './home';
import SearchPage from './searchPage';
import ExampleLogin from './exampleLogin';

export {
  AllReviewPage,
  ExamplePage,
  LoginPage,
  MovieDetailsPage,
  MyReviewPage,
  ProfilePage,
  RegisterPage,
  SplashScreen,
  HomePage,
  SearchPage,
  ExampleLogin,
};
