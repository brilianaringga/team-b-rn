import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  StatusBar,
  Image,
  ScrollView,
  moment,
  ReadMore,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  YoutubePlayer,
  Modal,
  useCallback,
  Pressable,
  Share,
} from 'libraries';
import { Text } from 'components';
import styles from './style';
import { appString, Color, FONTS, METRICS, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import { Icon } from 'react-native-eva-icons';
import {
  getMovieDetail,
  getSimilarMovie,
} from 'configs/redux/reducers/movies/moviesActions';
import {
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';

const MovieDetailsPage = (props) => {
  const {
    route,
    navigation,
    dispatchExample,
    contoh_email,
    contoh_data,
    dispatchGetApi,
    contoh_data_api,
    data_movie_detail,
    dispatchGetMovieDetail,
    isLoadingMovieDetail,
    dispatchGetSimilarMovie,
    data_similar,
  } = props;

  const { movieId, holdRender } = route.params;

  const [preventFirstRender, setPreventFirstRender] = useState(holdRender);
  const [urlTrailer, setUrlTrailer] = useState('');

  const closeModal = useCallback(() => showModal(false), []);

  useEffect(() => {
    // isi initial first load
    //contoh update state global via redux
    const payload = {
      params: {
        id: movieId,
      },
    };
    dispatchGetMovieDetail(payload);
    const payload2 = {
      params: {
        movie_id: movieId,
      },
    };
    dispatchGetSimilarMovie(payload2);
  }, []);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    if (data_movie_detail.hasOwnProperty('videos')) {
      if (data_movie_detail.videos.results) {
        if (data_movie_detail.videos.results.length > 0) {
          data_movie_detail.videos.results.forEach((element) => {
            if (element.hasOwnProperty('key')) {
              if (!element.key.includes('_') && !element.key.includes('-')) {
                setUrlTrailer(element.key);
              }
            }
          });
        }
      }
    }

    setPreventFirstRender(false);
  }, [data_movie_detail]);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
  }, [data_similar]);

  const shareSingleImage = async (item) => {
    const shareOptions = {
      title: `${item.title} `,
      message: `${item.overview}`,
      url: `https://image.tmdb.org/t/p/w500${data_movie_detail.backdrop_path}`,
      failOnCancel: false,
    };

    try {
      const ShareResponse = await Share.open(shareOptions);
      console.log(JSON.stringify(ShareResponse, null, 2));
      // setResult(JSON.stringify(ShareResponse, null, 2));
    } catch (error) {
      console.log('Error =>', error);
      // setResult('error: '.concat(getErrorString(error)));
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Color.black} barStyle={'light-content'} />
      {isLoadingMovieDetail || preventFirstRender ? (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size="large" color={Color.primaryColor} />
        </View>
      ) : (
        <View style={{ flex: 1 }}>
          <View style={{ marginBottom: scale(10), position: 'relative' }}>
            <Image
              style={{
                width: scale(METRICS.window.width),
                height: METRICS.screen.height * 0.3,
              }}
              source={{
                uri: `https://image.tmdb.org/t/p/w500${data_movie_detail.backdrop_path}`,
              }}
            />
            <View
              style={{
                backgroundColor: 'rgba(0,0,0,0.2)',
                position: 'absolute',
                top: scale(10),
                left: scale(10),
                borderRadius: scale(35),
                padding: scale(5),
              }}>
              <TouchableOpacity onPress={() => navigation.pop()}>
                <View>
                  <Icon
                    name={'arrow-ios-back-outline'}
                    height={scale(35)}
                    width={scale(35)}
                    fill={'white'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: METRICS.window.width,
                position: 'absolute',
                bottom: scale(-10),
                alignItems: 'flex-end',
                justifyContent: 'center',
              }}>
              <View style={{ flexDirection: 'row' }}>
                <TouchableNativeFeedback
                  onPress={() => {
                    shareSingleImage(data_movie_detail);
                  }}>
                  <View
                    style={{
                      backgroundColor: Color.primaryColor,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: scale(24),
                      marginRight: scale(8),
                      // height: scale(24),
                      // width: scale(24),
                      padding: scale(10),
                    }}>
                    <Icon
                      name="share-outline"
                      width={scale(24)}
                      height={scale(24)}
                      fill={'white'}
                    />
                  </View>
                </TouchableNativeFeedback>

                <Pressable
                  onPress={() => {
                    navigation.navigate('AllReviewPage', {
                      movieItem: data_movie_detail,
                    });
                  }}>
                  <View
                    style={{
                      backgroundColor: Color.primaryColor,
                      padding: scale(10),
                      minWidth: scale(10),
                      borderBottomLeftRadius: scale(35),
                      borderTopLeftRadius: scale(35),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Icon
                        name="message-circle-outline"
                        width={24}
                        height={24}
                        fill={'white'}
                      />
                      <Text
                        medium
                        style={{
                          color: 'white',
                          textAlign: 'center',
                          fontSize: scale(17),
                          marginLeft: scale(5),
                        }}>
                        {data_movie_detail.reviews}{' '}
                        {data_movie_detail.reviews > 1 ? 'Reviews' : 'Review'}
                      </Text>
                    </View>
                  </View>
                </Pressable>
              </View>
            </View>
          </View>
          <ScrollView style={{ flex: 1 }}>
            <View style={{ paddingHorizontal: scale(15) }}>
              <Text medium style={{ color: 'white', fontSize: scale(30) }}>
                {data_movie_detail.title}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                paddingHorizontal: scale(15),
              }}>
              <Icon
                name={'clock-outline'}
                height={scale(15)}
                width={scale(15)}
                fill={'#D2D4D4'}
              />
              <Text
                style={{
                  color: '#D2D4D4',
                  fontSize: scale(15),
                  marginLeft: scale(5),
                  marginRight: scale(10),
                }}>
                {data_movie_detail.runtime} minutes
              </Text>
              <Icon
                name={'star'}
                height={scale(15)}
                width={scale(15)}
                fill={Color.yellowStar}
              />
              <Text
                style={{
                  color: '#D2D4D4',
                  fontSize: scale(15),
                  marginHorizontal: scale(5),
                }}>
                {data_movie_detail.vote_average} (TMDB)
              </Text>
            </View>
            <View
              style={{
                width: METRICS.window.width,
                height: scale(0.7),
                backgroundColor: '#1E1E28',
                marginVertical: scale(15),
              }}></View>
            <View style={{ flexDirection: 'row', paddingLeft: scale(15) }}>
              <View style={{ marginRight: scale(45) }}>
                <Text medium style={{ color: 'white', fontSize: scale(20) }}>
                  Release Date
                </Text>

                <Text
                  style={{
                    color: '#D2D4D4',
                    fontSize: scale(15),
                  }}>
                  {moment(new Date(data_movie_detail.release_date)).format(
                    'LL',
                  )}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                }}>
                <Text
                  medium
                  style={{
                    color: 'white',
                    fontSize: scale(20),
                  }}>
                  Genre
                </Text>
                <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                  {data_movie_detail.genres.map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={{
                          padding: scale(5),
                          backgroundColor: '#3B3B43',
                          marginRight: scale(5),
                          borderRadius: scale(10),
                        }}>
                        <Text
                          style={{
                            color: '#D2D4D4',
                            fontSize: scale(15),
                          }}>
                          {item.name}
                        </Text>
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            </View>
            <View
              style={{
                width: METRICS.window.width,
                height: scale(0.7),
                backgroundColor: '#1E1E28',
                marginVertical: scale(15),
              }}></View>
            <View>
              <Text
                medium
                style={{
                  color: 'white',
                  fontSize: scale(20),
                  paddingHorizontal: scale(15),
                }}>
                Synopsis
              </Text>
              <ReadMore
                numberOfLines={4}
                style={{
                  color: '#D2D4D4',
                  fontSize: scale(15),
                  fontFamily: FONTS.brandonGrotesque,
                  paddingHorizontal: scale(15),
                }}
                seeMoreStyle={{
                  marginLeft: scale(5),
                  color: 'white',
                  fontSize: scale(15),
                  fontFamily: FONTS.brandonGrotesque,
                }}
                seeLessStyle={{
                  marginLeft: scale(5),
                  color: 'white',
                  fontSize: scale(15),
                  fontFamily: FONTS.brandonGrotesque,
                }}
                backgroundColor={Color.black}>
                {data_movie_detail.overview}
              </ReadMore>
            </View>
            {urlTrailer !== '' ? (
              <View>
                <View
                  style={{
                    marginVertical: scale(10),
                    paddingHorizontal: scale(15),
                  }}>
                  <Text medium style={{ color: 'white', fontSize: scale(20) }}>
                    Trailer
                  </Text>
                </View>
                <YoutubePlayer height={250} videoId={urlTrailer} />
              </View>
            ) : (
              <View></View>
            )}

            <View
              style={{
                marginVertical: scale(10),
                paddingHorizontal: scale(15),
              }}>
              <Text medium style={{ color: 'white', fontSize: scale(20) }}>
                Related Movies
              </Text>
            </View>
            <View style={{ paddingLeft: scale(15) }}>
              <FlatList
                showsHorizontalScrollIndicator={false}
                data={data_similar}
                renderItem={({ item }) => (
                  <View>
                    <View
                      style={{
                        backgroundColor: 'white',
                        height: scale(160),
                        width: scale(120),
                        marginRight: scale(10),
                        borderRadius: scale(5),
                      }}>
                      <Image
                        source={{
                          uri: `https://image.tmdb.org/t/p/w500${item.poster_path}`,
                        }}
                        style={{
                          height: scale(160),
                          width: scale(120),
                          marginRight: scale(10),
                          borderRadius: scale(5),
                        }}
                      />
                    </View>
                  </View>
                )}
                keyExtractor={(item, index) => index.toString()}
                horizontal
              />
            </View>
          </ScrollView>
        </View>
      )}
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    contoh_data: state.exampleStore.contoh_data,
    contoh_email: state.exampleStore.contoh_email,
    contoh_data_api: state.exampleStore.contoh_data_api,
    data_movie_detail: state.movieStore.data_movie_detail,
    isLoadingMovieDetail: state.movieStore.isLoadingMovieDetail,
    data_similar: state.movieStore.data_similar,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchExample: () => dispatch(exampleAction()),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
    dispatchGetMovieDetail: (payload) => dispatch(getMovieDetail(payload)),
    dispatchGetSimilarMovie: (payload) => dispatch(getSimilarMovie(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetailsPage);
