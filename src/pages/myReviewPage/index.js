/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
import { Button, Text as OrText } from 'components';
import {
  deleteReview,
  editReview,
  getUserReviews,
} from 'configs/redux/reducers/reviews/reviewActions';
import {
  Alert,
  Animated,
  BottomSheet,
  connect,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  React,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TextInput,
  useEffect,
  useIsFocused,
  useState,
  View,
} from 'libraries';
import { useRef } from 'react';
import { Icon } from 'react-native-eva-icons';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { AirbnbRating } from 'react-native-ratings';
import { Color, scale } from 'utils';

const Text = ({ children, style, ...props }) => {
  return (
    <OrText {...props} style={[style, { color: '#D2D4D4' }]}>
      {children}
    </OrText>
  );
};

const StripHandler = () => {
  return (
    <View
      style={{
        width: scale(125),
        height: scale(4),
        backgroundColor: 'gray',
        alignSelf: 'center',
        borderRadius: scale(2),
        marginVertical: scale(10),
      }}></View>
  );
};

const SheetEditReview = ({ sheetRef, review, ...props }) => {
  const { dispatchEditReviews, dispatchGetReviews } = props;
  const [rating, setRating] = useState(5);
  const [headline, setHeadline] = useState('');
  const [reviewDesc, setReviewDesc] = useState('');

  useEffect(() => {
    setRating(String(review.rate));
    setHeadline(review.headline);
    setReviewDesc(review.review_description);
  }, [review]);

  const editHandler = async () => {
    try {
      const payload = {
        paramsId: review.id,
        body: {
          rating: rating,
          headline: headline,
          review: reviewDesc,
        },
      };

      await dispatchEditReviews(payload);
      await dispatchGetReviews();
      sheetRef.current.snapTo(1);
    } catch (error) {
      Alert.Alert(error.message);
    }
  };

  return (
    <View
      style={{
        height: '100%',
        backgroundColor: Color.black,
        paddingHorizontal: scale(10),
        paddingTop: scale(10),
        alignItems: 'center',
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{ flex: 1, width: '100%' }}
        keyboardVerticalOffset={scale(120)}>
        <ScrollView
          style={{
            flex: 1,
            width: '100%',
          }}>
          <Text style={{ fontSize: scale(16), alignSelf: 'center' }}>
            GIVE YOUR REVIEWS
          </Text>
          <AirbnbRating
            count={10}
            size={20}
            reviews={[
              'Terrible',
              'Bad',
              'Meh',
              'OK',
              'Good',
              'Hmm...',
              'Very Good',
              'Wow',
              'Amazing',
              'Unbelievable',
            ]}
            defaultRating={rating}
            onFinishRating={(selectedRating) => {
              setRating(selectedRating);
            }}
          />
          <Text style={{ fontSize: scale(18), alignSelf: 'center' }}>
            Your rating is : {rating}
          </Text>
          <TextInput
            style={{
              backgroundColor: 'white',
              width: '100%',
              marginVertical: scale(10),
              padding: scale(14),
              borderRadius: scale(5),
            }}
            value={headline}
            placeholder="Write your headline for your review here"
            onChangeText={(value) => {
              setHeadline(value);
            }}
          />
          <TextInput
            style={{
              backgroundColor: 'white',
              width: '100%',
              height: scale(300),
              padding: scale(14),
              paddingTop: scale(14),
              borderRadius: scale(5),
              marginBottom: scale(10),
            }}
            value={reviewDesc}
            multiline={true}
            numberOfLines={5}
            placeholder="Write your review here"
            onChangeText={(value) => {
              setReviewDesc(value);
            }}
          />
          <View style={{ width: '100%' }}>
            <Button
              title="submit"
              textStyle={{ color: 'white' }}
              buttonStyle={{
                backgroundColor: Color.primaryColor,
                width: '100%',
                margin: 0,
                marginBottom: scale(10),
              }}
              onPress={() => {
                editHandler();
              }}
            />
            <Button
              title="cancel"
              textStyle={{ color: Color.primaryColor }}
              buttonStyle={{
                backgroundColor: Color.grey,
                width: '100%',
                margin: 0,
              }}
              onPress={() => {
                sheetRef.current.snapTo(1);
              }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

const MyReviewPage = (props) => {
  const {
    data_reviews,
    data_page,
    data_totalpages,
    data_totalresults,
    isLoading,
    error,
    dispatchGetReviews,
    dispatchDeleteReviews,
  } = props;

  const isScreenFocused = useIsFocused();
  const actionSheet = useRef(null);
  const deleteSheet = useRef(null);
  const editSheet = useRef(null);
  const [selectedReview, setSelectedReview] = useState({});
  const [fall] = useState(new Animated.Value(1));

  useEffect(() => {
    dispatchGetReviews();
  }, []);

  // useEffect(() => {
  //   if (isScreenFocused) {
  //     dispatchGetReviews();
  //   }
  // }, [isScreenFocused]);

  const deleteHandler = async () => {
    try {
      const payload = {
        paramsId: selectedReview.id,
      };

      await dispatchDeleteReviews(payload);
      await dispatchGetReviews();

      Alert.alert('review deleted');
      deleteSheet.current.snapTo(1);
    } catch (ers) {
      Alert.alert('something wrong');
    }
  };

  return (
    <>
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: Color.black,
        }}>
        <StatusBar backgroundColor={Color.black} barStyle="light-content" />
        <View
          style={{
            flex: 1,
            paddingHorizontal: scale(10),
            paddingTop: scale(14),
            paddingBottom: scale(50),
          }}>
          <Text style={{ fontSize: scale(28), fontWeight: 'bold' }}>
            Your Reviews . . .
          </Text>
          <FlatList
            data={data_reviews}
            keyExtractor={(item) => String(item.id)}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => {
              return (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: scale(50),
                  }}>
                  <Text style={{ fontSize: scale(20) }}>
                    You haven't review any movie
                  </Text>
                </View>
              );
            }}
            renderItem={({ item }) => {
              return (
                <View
                  style={{
                    backgroundColor: Color.grey,
                    borderRadius: scale(16),
                    marginBottom: scale(10),
                    padding: scale(10),
                  }}>
                  <View style={{ flexDirection: 'row' }}>
                    <Image
                      source={{
                        uri: `https://image.tmdb.org/t/p/w500${item.poster}`,
                      }}
                      style={{
                        width: scale(100),
                        height: scale(150),
                        borderRadius: scale(10),
                        marginRight: scale(10),
                      }}
                    />
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: scale(20), fontWeight: 'bold' }}>
                        {item.title}
                      </Text>
                      <Text style={{ fontSize: scale(14) }}>
                        {item.reviewDate}
                      </Text>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginBottom: 5,
                        }}>
                        <Icon
                          name="star"
                          width={24}
                          height={24}
                          fill={Color.yellowStar}
                          style={{ marginRight: 5 }}
                        />
                        <Text>{String(item.rate)}/10</Text>
                      </View>

                      <Text style={{ fontSize: scale(16), fontWeight: 'bold' }}>
                        {item.headline}
                      </Text>

                      <Text
                        numberOfLines={2}
                        ellipsizeMode="tail"
                        style={{ flex: 1, paddingRight: 10 }}>
                        {item.review_description}
                      </Text>

                      <TouchableOpacity
                        style={{
                          alignSelf: 'flex-end',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}
                        onPress={() => {
                          setSelectedReview({ ...item });
                          actionSheet.current.snapTo(1);
                          setTimeout(() => {
                            actionSheet.current.snapTo(0);
                          }, 100);
                        }}>
                        <Text>More</Text>
                        <Icon
                          name="arrowhead-right"
                          width={16}
                          height={16}
                          fill="white"
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              );
            }}
          />
        </View>
        {/* <Modal visible={true}>
          <ActivityIndicator color={Color.primaryColor} />
        </Modal> */}
      </SafeAreaView>
      <BottomSheet
        ref={actionSheet}
        snapPoints={[scale(300), 0]}
        initialSnap={1}
        borderRadius={15}
        style={{ flex: 1 }}
        callbackNode={fall}
        renderContent={() => {
          return (
            <View
              style={{
                height: scale(250),
                padding: scale(10),
                backgroundColor: Color.black,
              }}>
              <StripHandler />
              <Button
                title="edit"
                textStyle={{ color: 'white' }}
                onPress={() => {
                  actionSheet.current.snapTo(1);
                  editSheet.current.snapTo(0);
                }}
              />
              <Button
                title="delete"
                textStyle={{ color: 'white' }}
                buttonStyle={{ backgroundColor: Color.primaryColor }}
                onPress={() => {
                  actionSheet.current.snapTo(1);
                  deleteSheet.current.snapTo(0);
                }}
              />
              <Button
                title="cancel"
                textStyle={{ color: Color.primaryColor }}
                buttonStyle={{ backgroundColor: Color.grey }}
                onPress={() => {
                  actionSheet.current.snapTo(1);
                }}
              />
            </View>
          );
        }}
      />
      <BottomSheet
        ref={deleteSheet}
        snapPoints={[scale(230), 0]}
        initialSnap={1}
        borderRadius={15}
        style={{ flex: 1 }}
        enabledGestureInteraction={false}
        renderContent={() => {
          return (
            <View
              style={{
                height: scale(230),
                padding: scale(10),
                backgroundColor: Color.black,
              }}>
              <Text style={{ alignSelf: 'center', fontSize: scale(18) }}>
                Are you sure?
              </Text>
              <Button
                title="yes"
                textStyle={{ color: 'white' }}
                buttonStyle={{ backgroundColor: Color.primaryColor }}
                onPress={() => {
                  deleteHandler();
                }}
                isLoading={isLoading}
              />
              <Button
                title="no"
                textStyle={{ color: Color.primaryColor }}
                buttonStyle={{ backgroundColor: Color.grey }}
                onPress={() => {
                  deleteSheet.current.snapTo(1);
                  actionSheet.current.snapTo(0);
                }}
              />
            </View>
          );
        }}
      />
      <BottomSheet
        ref={editSheet}
        snapPoints={['88%', 0]}
        initialSnap={1}
        borderRadius={15}
        enabledGestureInteraction={false}
        renderContent={() => (
          <SheetEditReview
            {...props}
            review={selectedReview}
            sheetRef={editSheet}
          />
        )}
      />
      <Animated.View
        style={{
          ...StyleSheet.abosulteFilObbject,
          zIndex: 1,
          backgroundColor: 'f000',
          opacity: Animated.add(0.1, Animated.multiply(fall, 0.9)),
        }}
      />
    </>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    data_reviews: state.reviewStore.data_user_reviews,
    data_page: state.reviewStore.user_reviews_current_page,
    data_totalpages: state.reviewStore.user_reviews_current_totalpages,
    data_totalresults: state.reviewStore.user_reviews_current_totalresults,
    isLoading: state.reviewStore.isLoading,
    error: state.reviewStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetReviews: () => dispatch(getUserReviews()),
    dispatchEditReviews: (payload) => dispatch(editReview(payload)),
    dispatchDeleteReviews: (payload) => dispatch(deleteReview(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyReviewPage);
