import { React, useEffect, useState, connect, View, ImageBackground, TouchableOpacity, } from 'libraries';
import { Text, Button,TextInput } from 'components';
import styles from './style';
import { appString, Color, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import IMG from 'assets/images';
import LotieFiles from 'assets/animasi';
import LottieView from 'lottie-react-native'
import { RegisterAction } from 'configs/redux/reducers/auth/authActions';


const RegisterPage = (props) => {
  const {
    navigation,
    dispatchExample,
    contoh_email,
    contoh_data,
    dispatchGetApi,
    contoh_data_api,
    dispatchRegister,
    errorRegister
  } = props;

const [username, setUserName] = useState('')
const [fullname, setFullName] = useState('')
const [email, setEmail] = useState('')
const [password, setPassword] = useState('')
const [isValid, setIsValid] = useState({
  username: true,
  fullname: true,
  email: true,
  password: true
})

  useEffect(() => {
    // isi initial first load
    //contoh update state global via redux
    dispatchExample();
    const start = 1;
    const limit = 2;
    dispatchGetApi(start, limit);
  }, []);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    if (errorRegister) {
       console.log(errorRegister.status);
       if (errorRegister.status == 422) {
         alert(errorRegister.data.error.message)
       }
    }
  }, [errorRegister]);

const submitRegister = () =>{
  
  const check = validation()
  // if (check) {
  //     const payload={
  //       body: {
  //       username,
  //       fullname,
  //       password,
  //       email
  //     }
  //   }
  //   dispatchRegister({
  //     body: {
  //     username,
  //     fullname,
  //     password,
  //     email
  //   }
  // })
  // }

  console.log(isValid)
}

const validation = () => {
  const isUserName = /^[a-z0-9]+$/i.test(username);
  const isFullname = /^[a-z\d\-_\s]+$/i.test(fullname);
  const isEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email);
  const isPassword = /^\S*$/.test(password)
  setIsValid({
    ...isValid, 
    username: isUserName,
    fullname: isFullname,
    email: isEmail,
    password: isPassword
  })
  console.log(isFullname) 
    if (
      isUserName && isFullname && isPassword && isEmail
    ) {
      console.log({
        body: {
        username,
        fullname,
        password,
        email
      }
    })
      dispatchRegister({
        body: {
        username,
        fullname,
        password,
        email
      }
    })
    }


  // if (type == 'username') {
  //   setIsValid({
  //     ...isValid, username: /^[a-z0-9]+$/i.test(value)
  //   })
  // } else if (type == 'fullname') {
  //   setIsValid({
  //     ...isValid, fullname: /^[a-z\d\-_\s]+$/i.test(value)
  //   })
  // } else if (type == 'email') {
  //   setIsValid({
  //     ...isValid, email: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(value)
  // }) 
  // } else {
  //   setIsValid({
  //     ...isValid, password: /^\S*$/.test(value)
  //   })
  // }
}

  return (
    <ImageBackground style={{flex:1}} source={IMG.background}>
      <View style={styles.lotiecontainer}>
        <LottieView source={LotieFiles.movie1} autoPlay loop/>
      </View>
      {/* <Text>Ini Example Page</Text>
      <Text>contoh ambil redux</Text>
      <Text>{contoh_email}</Text>
      <Text>{contoh_data}</Text>

      <Text>
        Contoh ambil data dari appString yang sudah di define di utils/strings
      </Text>
      <Text>{appString.pages.home.title}</Text> */}
      <View style={styles.textinputcontainer}>
        <TextInput pattern={[
            '/^[a-z0-9]+$/i'
          ]}
          onValidation={check => setIsValid({...isValid, username:check})} onChangeText={(text) => {  
          setUserName(text)
        }} placeholder={"Username"}/>
        {(!isValid.username)?<Text style={{color: Color.primaryColor,marginLeft:scale(50),
    marginRight:scale(50),}}>accept only alpha numeric</Text>:<View></View>}
        <TextInput onChangeText={(text) => {
          setFullName(text)
        }} placeholder={"Fullname"}/>
        {(!isValid.fullname)?<Text style={{color: Color.primaryColor, marginLeft:scale(50),
    marginRight:scale(50),}}>accept only alpha space</Text>:<View></View>}
        <TextInput onChangeText={(text) => {
          setEmail(text)
        }} placeholder={"Email"}/>
        {(!isValid.email)?<Text style={{color: Color.primaryColor, marginLeft:scale(50),
    marginRight:scale(50),}}>accept only email type</Text>:<View></View>}
        <TextInput onChangeText={(text) => {
          setPassword(text)
        }} placeholder={"Password"}/>
        {(!isValid.password)?<Text style={{color: Color.primaryColor, marginLeft:scale(50),
    marginRight:scale(50),}}>accept only non whitespace</Text>:<View></View>}
      </View>
      <Button
        title='SIGN UP'
        onPress={() => {submitRegister()}}
      />
      <TouchableOpacity onPress={() => {
        navigation.navigate('LoginPage')
      }} >
        <Text style={styles.text}>Already have an account? Sign IN</Text>
      </TouchableOpacity>
    </ImageBackground>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    contoh_data: state.exampleStore.contoh_data,
    contoh_email: state.exampleStore.contoh_email,
    contoh_data_api: state.exampleStore.contoh_data_api,
    errorRegister: state.authStore.errorRegister,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchExample: () => dispatch(exampleAction()),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
    dispatchRegister: (payload) => dispatch(RegisterAction(payload))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterPage);
