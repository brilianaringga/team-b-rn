import { StyleSheet } from "react-native";
import { Color, scale, METRICS } from "utils";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.black,
  },
  textinputcontainer: {
    // flex:1
    marginTop: scale(METRICS.window.height*0.4),
    marginBottom: scale(30)
  },
  text: {
    color: Color.white,
    textAlign:'center'
  },
  lotiecontainer: {
    // flex:1,
    justifyContent:'center',

  }
});

export default styles;
