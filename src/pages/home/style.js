import { StyleSheet } from 'react-native';
import { Color, METRICS, scale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.black,
  },
  carouselContainer: {
    marginTop: scale(10),
  },
  itemContainer: {
    width: Math.round(METRICS.screen.width * 0.6),
    height: Math.round((METRICS.screen.width * 3) / 4),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'gray',
    borderRadius: scale(15),
    position: 'relative',
  },
  illustrationImage: {
    width: Math.round(METRICS.screen.width * 0.6),
    height: Math.round((METRICS.screen.width * 3) / 4),
    borderRadius: scale(15),
  },
  itemLabel: {
    color: 'white',
    fontSize: 24,
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  paginationContainer: {
    paddingVertical: 8,
  },
  paginationDot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: -5,
  },
});

export default styles;
