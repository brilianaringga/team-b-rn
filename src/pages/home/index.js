import {
  React,
  useEffect,
  connect,
  View,
  Carousel,
  useRef,
  useState,
  Pagination,
  Image,
  StatusBar,
  changeNavigationBarColor,
  FlatList,
  FlatGrid,
  TouchableNativeFeedback,
  SectionGrid,
  showMessage,
  ScrollView,
  SkeletonPlaceholder,
  moment,
  Pressable,
  useCallback,
} from 'libraries';
import { SafeAreaView } from 'react-native-safe-area-context';

import { SkeletonRow, SkeletonShowcase, Text } from 'components';
import styles from './style';
import { appString, Color, METRICS, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import { scrollInterpolator, animatedStyles } from 'utils';
import { Icon } from 'react-native-eva-icons';
import {
  getGenreList,
  getMovieListbyGenre,
  getPopularMovie,
  getTrendingMovie,
  getUpComingMovie,
  getTopRateMovie,
} from 'configs/redux/reducers/movies/moviesActions';

const SLIDER_WIDTH = METRICS.window.width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.6);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 4);

const showCaseSkeleton = [1, 2, 3];

const HomePage = (props) => {
  const {
    navigation,
    error,
    dispatchGetGenre,
    data_genre,
    data_movie_genre,
    data_trending,
    data_popular,
    dispatchGetMovieListGenre,
    dispatchGetTrendingMovie,
    dispatchGetPopularMovie,
    dispatchGetUpcomingMovie,
    data_upcoming,
    dispatchGetTopRateMovie,
    data_topRate,
  } = props;
  const ref = useRef(null);
  const [state, setState] = useState({
    index: 0,
    selectedCategoryIndex: 0,
  });

  const getCarouselData = async (payload) => {
    return dispatchGetMovieListGenre(payload);
  };

  useEffect(() => {
    Promise.all([
      getCarouselData({
        params: {
          page: 1,
          genre_id: 35, //get Comedy Movie for Caraosel,
        },
      }),
      dispatchGetGenre(),
      dispatchGetTrendingMovie(),
      dispatchGetPopularMovie(),
      dispatchGetUpcomingMovie(),
      dispatchGetTopRateMovie(),
    ]);
    changeNavigationBarColor(Color.black);
    try {
    } catch (e) {
      console.log('Error : ', e);
    }
  }, []);

  // useEffect(() => {
  //   if (data_genre.length > 0) {
  //     const payload = {
  //       params: {
  //         page: 1,
  //         genre_id: 35, //get Comedy Movie for Caraosel,
  //       },
  //     };
  //     dispatchGetMovieListGenre(payload);
  //   }
  // }, [data_genre]);

  const renderMovieItem = useCallback(({ item }) => {
    return (
      <Pressable
        onPress={() => {
          navigation.navigate('MovieDetailsPage', {
            movieId: item.id,
            holdRender: true,
          });
        }}>
        <View
          style={{
            backgroundColor: 'white',
            height: scale(160),
            width: scale(120),
            marginRight: scale(10),
            borderRadius: scale(5),
            position: 'relative',
          }}>
          <Image
            source={{
              uri: `https://image.tmdb.org/t/p/w300${item.poster_path}`,
            }}
            style={{
              height: scale(160),
              width: scale(120),
              marginRight: scale(10),
              borderRadius: scale(5),
            }}
          />
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              backgroundColor: 'rgba(0,0,0,0.35)',
              width: scale(120),
              minHeight: scale(30),
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              medium
              style={{
                fontSize: scale(15),
                color: 'white',
                textShadowColor: 'rgba(0, 0, 0, 0.85)',
                textShadowOffset: { width: 1, height: 1 },
                textShadowRadius: 20,
                textAlign: 'center',
              }}>
              {item.title} ({moment(new Date(item.release_date)).format('Y')})
            </Text>
          </View>
        </View>
      </Pressable>
    );
  }, []);

  const renderShowcase = useCallback(({ item }) => {
    return (
      <Pressable
        onPress={() => {
          navigation.navigate('MovieDetailsPage', {
            movieId: item.id,
            holdRender: true,
          });
        }}>
        <View style={styles.itemContainer}>
          <Image
            source={{
              uri: `https://image.tmdb.org/t/p/w300${item.poster_path}`,
            }}
            style={styles.illustrationImage}
          />
          <View
            style={{
              position: 'absolute',
              top: scale(10),
              right: scale(10),
              backgroundColor: 'rgba(255,255,255,0.6)',
              paddingHorizontal: scale(10),
              paddingVertical: scale(1),
              borderRadius: scale(10),
              minWidth: scale(50),
            }}>
            <Text bold style={{ fontSize: scale(8) }}>
              TMDB
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon
                name={'star'}
                height={scale(15)}
                width={scale(15)}
                fill={Color.yellowStar}
              />
              <Text bold>{item.vote_average}</Text>
            </View>
          </View>
          <View
            style={{
              position: 'absolute',
              bottom: 0,
              width: '100%',
              backgroundColor: 'rgba(0,0,0,0.35)',
              alignItems: 'center',
              paddingVertical: scale(15),
              borderBottomLeftRadius: scale(15),
              borderBottomRightRadius: scale(15),
              borderTopLeftRadius: scale(10),
              borderTopRightRadius: scale(10),
            }}>
            <Text
              bold
              style={{
                fontSize: scale(20),
                color: 'white',
                textShadowColor: 'rgba(0, 0, 0, 0.85)',
                textShadowOffset: { width: 1, height: 1 },
                textShadowRadius: 20,
              }}>
              {item.title}
            </Text>
          </View>
        </View>
      </Pressable>
    );
  }, []);

  const keyExtractor = useCallback((item, index) => index.toString(), []);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Color.black} barStyle={'light-content'} />
      <View
        style={{
          position: 'relative',
        }}>
        <Text
          bold
          style={{
            color: Color.primaryColor,
            textAlign: 'center',
            fontSize: scale(30),
          }}>
          Bi-Movies
        </Text>
      </View>
      <ScrollView>
        <View style={{ marginBottom: scale(10) }}>
          {data_movie_genre.length > 0 ? (
            <View>
              <Carousel
                ref={ref}
                data={data_movie_genre}
                renderItem={renderShowcase}
                sliderWidth={SLIDER_WIDTH}
                itemWidth={ITEM_WIDTH}
                containerCustomStyle={styles.carouselContainer}
                inactiveSlideShift={0}
                onSnapToItem={(index) => setState({ ...state, index })}
                scrollInterpolator={scrollInterpolator}
                slideInterpolatedStyle={animatedStyles}
                useScrollView={true}
                loop={true}
                loopClonesPerSide={data_movie_genre.length}
                inactiveSlideScale={0.8}
                initialScrollIndex={data_movie_genre.length}
              />
            </View>
          ) : (
            <View style={{ marginTop: scale(10) }}>
              <SkeletonShowcase />
            </View>
          )}
        </View>

        <View style={{ marginLeft: scale(5), marginTop: scale(10) }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text bold style={{ color: 'gray', fontSize: scale(15) }}>
                TRENDING MOVIES
              </Text>
              <View
                style={{
                  width: scale(60),
                  height: scale(3),
                  backgroundColor: Color.primaryColor,
                  marginBottom: scale(5),
                  borderRadius: scale(5),
                }}></View>
            </View>
          </View>
        </View>
        <View
          style={{
            paddingLeft: scale(5),
          }}>
          {data_trending.length > 0 ? (
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={data_trending}
              renderItem={renderMovieItem}
              keyExtractor={keyExtractor}
              horizontal
            />
          ) : (
            <SkeletonRow
              count={5}
              backgroundColor={'#24242C'}
              highlightColor={'gray'}
              width={scale(120)}
              height={scale(160)}
              borderRadis={scale(5)}
              marginRight={scale(10)}
            />
          )}
        </View>
        <View style={{ marginLeft: scale(5), marginTop: scale(10) }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text bold style={{ color: 'gray', fontSize: scale(15) }}>
                TOP RATE MOVIES
              </Text>
              <View
                style={{
                  width: scale(60),
                  height: scale(3),
                  backgroundColor: Color.primaryColor,
                  marginBottom: scale(5),
                  borderRadius: scale(5),
                }}></View>
            </View>
          </View>
        </View>

        <View
          style={{
            paddingLeft: scale(5),
          }}>
          {data_topRate.length > 0 ? (
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={data_topRate}
              renderItem={renderMovieItem}
              keyExtractor={keyExtractor}
              horizontal
            />
          ) : (
            <SkeletonRow
              count={5}
              backgroundColor={'#24242C'}
              highlightColor={'gray'}
              width={scale(120)}
              height={scale(160)}
              borderRadis={scale(5)}
              marginRight={scale(10)}
            />
          )}
        </View>
        <View style={{ marginLeft: scale(5), marginTop: scale(10) }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text bold style={{ color: 'gray', fontSize: scale(15) }}>
                UPCOMING MOVIES
              </Text>
              <View
                style={{
                  width: scale(60),
                  height: scale(3),
                  backgroundColor: Color.primaryColor,
                  marginBottom: scale(5),
                  borderRadius: scale(5),
                }}></View>
            </View>
          </View>
        </View>

        <View
          style={{
            paddingLeft: scale(5),
          }}>
          {data_upcoming.length > 0 ? (
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={data_upcoming}
              renderItem={renderMovieItem}
              keyExtractor={keyExtractor}
              horizontal
            />
          ) : (
            <SkeletonRow
              count={5}
              backgroundColor={'#24242C'}
              highlightColor={'gray'}
              width={scale(120)}
              height={scale(160)}
              borderRadis={scale(5)}
              marginRight={scale(10)}
            />
          )}
        </View>

        <View style={{ height: scale(60) }}></View>
      </ScrollView>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    data_genre: state.movieStore.data_genre,
    data_movie_genre: state.movieStore.data_movie_genre,
    data_trending: state.movieStore.data_trending,
    data_popular: state.movieStore.data_popular,
    data_upcoming: state.movieStore.data_upcoming,
    data_topRate: state.movieStore.data_topRate,
    error: state.movieStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetGenre: () => dispatch(getGenreList()),
    dispatchGetMovieListGenre: (payload) =>
      dispatch(getMovieListbyGenre(payload)),
    dispatchGetTrendingMovie: () => dispatch(getTrendingMovie()),
    dispatchGetPopularMovie: () => dispatch(getPopularMovie()),
    dispatchGetUpcomingMovie: () => dispatch(getUpComingMovie()),
    dispatchGetTopRateMovie: () => dispatch(getTopRateMovie()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
