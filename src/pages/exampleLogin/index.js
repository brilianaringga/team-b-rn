import {
  React,
  useEffect,
  useState,
  connect,
  View,
  TextInput,
} from 'libraries';

import { Button, Text } from 'components';
import styles from './style';
import { appString, scale } from 'utils';
import { LoginAction } from 'configs/redux/reducers/auth/authActions';

const ExampleLogin = (props) => {
  const { navigation, dispatchLogin, isLoading } = props;

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const doLogin = () => {
    try {
      const payload = {
        body: {
          username,
          password,
        },
      };
      console.log(payload);
      dispatchLogin(payload);
    } catch (e) {}
  };

  useEffect(() => {}, []);

  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'Input Username'}
        onChangeText={(text) => {
          setUsername(text);
        }}
      />
      <TextInput
        placeholder={'Input Password'}
        onChangeText={(text) => {
          setPassword(text);
        }}
      />
      <View style={{ marginTop: scale(20) }}>
        <Button
          title={'Login'}
          onPress={() => {
            console.log('pencet');
            doLogin();
          }}
          isLoading={isLoading}
        />
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    isLoading: state.authStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchLogin: (payload) => dispatch(LoginAction(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExampleLogin);
