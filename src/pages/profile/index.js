import { React, 
  useEffect, 
  useState, 
  connect, 
  View, 
  SafeAreaView, 
  Image,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  StatusBar,
  changeNavigationBarColor,
} from 'libraries';
import { Text,Button } from 'components';
import styles from './style';
import { appString, Color, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import { TextInput } from 'react-native-paper'
import { GetProfileAction, LogoutAction, UpdateImageAction, UpdateProfileAction, OnChangeFullname, OnChangePassword} from 'configs/redux/reducers/profile/profileActions';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
//import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import *as ImagePicker from 'react-native-image-picker'
import {Form,Input, Item, Label} from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';

const ProfilePage = (props) => {
  const {
    
    dispatchOnFullname,
    dispatchOnPassword,
    dispatchUpdateProfil,
    dispatchLogout,
    dispatchGetProfile,
    dispatchUpdateImage,
    navigation,
    dispatchExample,
    contoh_email,
    contoh_data,
    dispatchGetApi,
    contoh_data_api,
    email,
    fullname,
    username,
    profile_picture,
    password
  } = props;


  const [MyPassword,setMyPassword]=useState(null);
  const [MyNama,setMyNama]=useState(null);
  

  useEffect(() => {
    changeNavigationBarColor(Color.black);
    dispatchGetProfile();
    //getNama();
    // isi initial first load
    //contoh update state global via redux
    // dispatchExample();
    // const start = 1;
    // const limit = 2;
    // dispatchGetApi(start, limit);
    //console.log('test')
    //console.log(email)

  },[]);

  useEffect(() => {
    dispatchGetProfile();
    
    //setMyNama (fullname)
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    //console.log(contoh_data_api);
  },[]);


  uploadImage = () => {
    ImagePicker.launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: 200,
        maxWidth: 200,
      },
      (response) => {
        //console.log('test')
        //console.log(response.uri)
        //setResponse(response);
        const form = new FormData() 
        form.append('profile_picture',{
          uri:response.uri,
          type:'image/jpg',
          name:'avatar.jpg'
        })
        const payload = {
          body: form,
          type: 'form-data'
        }
        //console.log(payload)
        dispatchUpdateImage(payload)
      },
    )

  }


  doLogout =() => {
    dispatchLogout()
    navigation.reset({
      index:0,
      routes: [{name:'LoginPage'}]})
  }

  doSave =() => {
    const payload = {
      body:{
        fullname:fullname,
        password:password,
      }
    }
    dispatchUpdateProfil(payload)
  
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
      {/* <KeyboardAvoidingView 
          keyboardVerticalOffset={250}
          behavior='padding'
          style={{flex:1}}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}

      
      <View style={{flex:1, justifyContent:'center'}}>
      <StatusBar backgroundColor={Color.black} barStyle={'light-content'} />
      <View style={{flexDirection:'row', justifyContent:'center', marginTop: scale(20), marginBottom: scale(0)}}>
      <Image 
      source={{uri: profile_picture == "" ? 'https://ualr.edu/studentaffairs/files/2020/01/blank-picture-holder.png':'http://139.59.124.53:3003'+profile_picture}}
      style={{width: scale(150),height: scale(150), borderRadius : scale (150)}}></Image>
      <ActionButton 
      buttonColor='red' 
      offsetY={10} 
      offsetX={100} 
      size={50}
      onPress = {uploadImage}
      >
      </ActionButton>

      </View>

      <View style={{justifyContent:'center', marginHorizontal: scale(30)}}>

          <Form style={{borderColor:'white'}}>
            <Item  style={{borderColor:'white'}} floatingLabel>
              <Label style={{color:'white'}} >Name</Label>
              <Input style={{color:'white'}} placeholderTextColor='white' autoCapitalize='none'
              onChangeText={(nama) => {
                const payload = {
                  fullname:nama
                }
                dispatchOnFullname(payload)
                //setMyNama(fullname)
              }}
              value={fullname}
              />
            </Item>

            <Item style={{borderColor:'white'}} floatingLabel>
              <Label style={{color:'white'}}>Username</Label>
              <Input style={{color:'white'}} placeholderTextColor='white'autoCapitalize='none' 
              onChangeText={(nickname) => setMyNickname(nickname)}
              value={username}
              />
            </Item>

            <Item style={{borderColor:'white'}} floatingLabel>
              <Label style={{color:'white'}}>Email</Label>
              <Input style={{color:'white'}} placeholderTextColor='white'autoCapitalize='none' 
              onChangeText={(email) => setMyEmail(email)}
              value={email}
              />
            </Item>

            <Item  style={{borderColor:'white'}} floatingLabel>
              <Label style={{color:'white'}} >Password</Label>
              <Input style={{color:'white'}} placeholderTextColor='white' autoCapitalize='none' secureTextEntry={true}
              onChangeText={(pass) => {
                const payload = {
                  password:pass
                }
                dispatchOnPassword(payload)
                //setMyPassword(pass)
              }}
              value={password}
              />
            </Item>
          </Form>

          {/* <TextInput 
            theme={{roundness:50,colors:{primary:'#FF002E'}}}
            style={{marginTop:5}}
            //secureTextEntry={true}
            label = "Name"
            mode = "outlined"
            onChangeText={(nama) => {
              const payload = {
                fullname:nama
              }
              dispatchOnFullname(payload)
              //setMyNama(fullname)
            }}
            value={fullname}></TextInput>
            <TextInput 
            editable={false}
            theme={{roundness:50,colors:{primary:'#FF002E'}}}
            style={{marginTop:5}}
            label = "Nickname"
            mode = "outlined"
            onChangeText={(nickname) => setMyNickname(nickname)}
            value={username}></TextInput>
          <TextInput 
            editable={false}
            theme={{roundness:50,colors:{primary:'#FF002E'}}}
            style={{marginTop:5}}
            label = "Email"
            mode = "outlined"
            onChangeText={(email) => setMyEmail(email)}
            value={email}></TextInput>
          <TextInput 
            theme={{roundness:50,colors:{primary:'#FF002E'}}}
            style={{marginTop:5}}
            secureTextEntry={true}
            label = "Password"
            mode = "outlined"
            onChangeText={(pass) => {
              const payload = {
                password:pass
              }
              dispatchOnPassword(payload)
              //setMyPassword(pass)
            }}
            value={password}></TextInput> */}
         </View>

         <View style={{flex:1, alignItems:'center',marginTop:scale(20)}}>
         
         <Button 
         buttonStyle={{padding: scale(5)}}
         textStyle={{fontSize:scale(15)}}
         title={'SAVE'} onPress={()=> doSave()}></Button>
         <Button 
         buttonStyle={{padding: scale(5)}}
         textStyle={{fontSize:scale(15)}}
         title={'LOGOUT'} onPress={()=> doLogout()}></Button>
        
        
        
        
         {/* <Button mode='contained'
              theme=
              {{roundness:50,colors:{primary:'#FF002E'}}}

              labelStyle={{fontSize:scale(20),}}
              style={{width:scale(150), height:scale(50)}}
              onPress={doSave}>
              SAVE
            </Button>
         <Button mode='contained'
              theme={{roundness:50,colors:{primary:'#FF002E'}}}
              labelStyle={{fontSize:scale(20),}}
              style={{width:scale(150),height: scale(50) }}
              onPress={doLogout}>
              LOGOUT
            </Button> */}
          
        </View>
        </View>
        {/* </TouchableWithoutFeedback>
        </KeyboardAvoidingView> */}
        </ScrollView>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    // contoh_data: state.exampleStore.contoh_data,
    // contoh_email: state.exampleStore.contoh_email,
    // contoh_data_api: state.exampleStore.contoh_data_api,
    email:state.profileStore.email,
    password:state.profileStore.password,
    fullname:state.profileStore.fullname,
    username:state.profileStore.username,
    profile_picture:state.profileStore.profile_picture,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    // dispatchExample: () => dispatch(exampleAction()),
    // dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
    dispatchGetProfile: () => dispatch(GetProfileAction()),
    dispatchUpdateImage: (payload) => dispatch(UpdateImageAction(payload)),
    dispatchUpdateProfil: (payload) => dispatch(UpdateProfileAction(payload)),
    dispatchOnFullname:(payload) => dispatch(OnChangeFullname(payload)),
    dispatchOnPassword:(payload) => dispatch(OnChangePassword(payload)),
    dispatchLogout: () => dispatch(LogoutAction())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
