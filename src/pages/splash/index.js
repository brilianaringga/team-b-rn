import { React, useEffect, connect, View, LottieView, Image, AsyncStorage } from 'libraries';
import { Text } from 'components';
import styles from './style';
import IMG from 'assets/images';
import { checkTokenValidation } from 'configs/redux/reducers/auth/authActions';

const SplashScreen = (props) => {
  const {
    navigation,
    dispatchCheckToken,
  } = props;

  useEffect(() => {
    setTimeout(() => {
      checkValidation();
    }, 2000);
  }, []);

 const checkValidation = async () => {
  const token = await AsyncStorage.getItem('@token');
  if(token === null) {
    navigation.reset({
      index: 0,
      routes: [{ name: 'LoginPage' }],
    });
  }
  else {
    try {
      dispatchCheckToken();
    }
    catch(e) {
      console.log(e);
    }
  }
 }

  return (
    <View style={styles.container}>
      <Image source={IMG.logo} style={styles.image} />
      <Text bold style={styles.text}>
        Bi-Movies
      </Text>
    </View>
  );
};

function mapDispatchToProps(dispatch) {
  return {
    dispatchCheckToken: () => dispatch(checkTokenValidation()),
  };
}

export default connect(null, mapDispatchToProps)(SplashScreen);
