import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 119,
    width: 134,
  },
  text: {
    color: Color.primaryColor,
    textAlign: 'center',
    fontSize: scale(30),
    marginTop: 20,
  },
});

export default styles;
