/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import { Button, Text as OrText } from 'components';
import {
  getMovieReviews,
  getUserReviews,
  submitReview,
} from 'configs/redux/reducers/reviews/reviewActions';
import {
  ActivityIndicator,
  BottomSheet,
  connect,
  Dimensions,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  React,
  ScrollView,
  SkeletonPlaceholder,
  StatusBar,
  TextInput,
  TouchableOpacity,
  useEffect,
  useState,
  View,
} from 'libraries';
import { useRef } from 'react';
import { Icon } from 'react-native-eva-icons';
import { AirbnbRating, Rating } from 'react-native-ratings';
import { Color, METRICS, scale, verticalScale } from 'utils';
import Skeleton from './skeleton';
import styles from './style';

const MARGIN_BOTTOM = 10;

const Text = ({ children, style, ...props }) => {
  return (
    <OrText {...props} style={[style, { color: '#D2D4D4' }]}>
      {children}
    </OrText>
  );
};

const StripHandler = () => {
  return (
    <View
      style={{
        width: scale(125),
        height: scale(4),
        backgroundColor: 'gray',
        alignSelf: 'center',
        borderRadius: scale(2),
        marginVertical: scale(10),
      }}
    />
  );
};

const CardReview = ({
  rating,
  headline,
  username,
  profilePicture,
  review,
  updatedAt,
  cardStyles,
  onBtnClick,
}) => {
  return (
    <View style={cardStyles.card}>
      <View style={cardStyles.cardHeader}>
        {profilePicture ? (
          <Image
            style={cardStyles.reviewAvatar}
            source={{
              uri: `http://139.59.124.53:3003/files/profile/thumbnail/${profilePicture}`,
            }}
            onError={() => {
              return (
                <View
                  style={{
                    ...cardStyles.reviewAvatar,
                    backgroundColor: 'gray',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Icon
                    name="person"
                    height={scale(30)}
                    width={scale(30)}
                    fill={Color.black}
                  />
                </View>
              );
            }}
          />
        ) : (
          <View
            style={{
              ...cardStyles.reviewAvatar,
              backgroundColor: 'gray',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon
              name="person"
              height={scale(30)}
              width={scale(30)}
              fill={Color.black}
            />
            {/* <Text>Masuk</Text> */}
          </View>
        )}

        <View style={cardStyles.cardHeaderContent}>
          <View style={cardStyles.cardHeaderScore}>
            <Icon name="star" height={24} width={24} fill={Color.yellowStar} />
            <Text style={cardStyles.score}>
              {rating ? rating : '-'}
              /10
            </Text>
            <Text
              style={{
                fontSize: scale(18),
                fontWeight: 'bold',
              }}>
              {headline}
            </Text>
          </View>
          <View style={cardStyles.cardHeaderReviewer}>
            <Text style={cardStyles.defaultText}>Reviewer : </Text>
            <Text style={cardStyles.reviewer}>{username}</Text>
          </View>
        </View>
      </View>
      <Text ellipsizeMode="tail" numberOfLines={3} style={cardStyles.content}>
        {review}
      </Text>
      <View>
        <Button
          title="details"
          textStyle={{
            color: 'white',
            fontSize: scale(16),
          }}
          buttonStyle={{
            backgroundColor: Color.primaryColor,
            padding: scale(10),
            minHeight: 0,
          }}
          onPress={() => onBtnClick()}
        />
      </View>
    </View>
  );
};

const SheetAddReview = (props) => {
  const {
    original_title: movie_title,
    id: movie_id,
    release_date: movie_release_date,
    backdrop_path: movie_picture_path,
  } = props.route.params.movieItem;
  const {
    sheetRef,
    dispatchGetReviews,
    dispatchMyReviews,
    dispatchSubmitReview,
    isLoading,
  } = props;
  const [rating, setRating] = useState(5);
  const [headline, setHeadline] = useState('');
  const [review, setReview] = useState('');

  const submitReviewHandler = async () => {
    try {
      const payload = {
        body: {
          id_movie: parseInt(movie_id),
          movie_title: movie_title,
          movie_release_date: movie_release_date,
          movie_picture_path: movie_picture_path,
          rating: parseInt(rating),
          headline: headline,
          review: review,
        },
      };

      // submit data
      await dispatchSubmitReview(payload);
      await dispatchGetReviews({
        paramsId: movie_id,
      });

      sheetRef.current.snapTo(2);
      // clear form
      setRating(5);
      setHeadline('');
      setReview('');
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View
      style={{
        height: '100%',
        backgroundColor: Color.grey,
      }}>
      <StripHandler />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{ flex: 1, width: '100%' }}
        keyboardVerticalOffset={verticalScale(60)}>
        <ScrollView>
          <View
            style={{
              alignItems: 'center',
              padding: 10,
            }}>
            <Text style={{ fontSize: scale(16) }}>GIVE YOUR REVIEWS</Text>
            <AirbnbRating
              count={10}
              size={20}
              reviews={[
                'Terrible',
                'Bad',
                'Meh',
                'OK',
                'Good',
                'Hmm...',
                'Very Good',
                'Wow',
                'Amazing',
                'Unbelievable',
              ]}
              defaultRating={5}
              onFinishRating={(selectedRating) => {
                setRating(selectedRating);
              }}
            />
            <Text style={{ fontSize: scale(18) }}>
              Your rating is : {rating}
            </Text>
            <TextInput
              style={{
                backgroundColor: 'white',
                width: '100%',
                marginVertical: scale(10),
                padding: scale(14),
                borderRadius: scale(5),
              }}
              value={headline}
              placeholder="Write your headline for your review here"
              onChangeText={(value) => {
                setHeadline(value);
              }}
            />
            <TextInput
              style={{
                backgroundColor: 'white',
                width: '100%',
                height: scale(125),
                padding: scale(14),
                paddingTop: scale(14),
                borderRadius: scale(5),
                marginBottom: scale(10),
              }}
              value={review}
              multiline={true}
              numberOfLines={5}
              placeholder="Write your review here"
              onChangeText={(value) => {
                setReview(value);
              }}
            />
            <View style={{ width: '100%' }}>
              <Button
                title="submit"
                textStyle={{ color: 'white' }}
                isLoading={isLoading}
                buttonStyle={{
                  backgroundColor: Color.primaryColor,
                  width: '100%',
                  margin: 0,
                  marginBottom: scale(10),
                }}
                onPress={() => submitReviewHandler()}
              />
              <Button
                title="cancel"
                textStyle={{ color: Color.primaryColor }}
                buttonStyle={{
                  backgroundColor: 'transparent',
                  width: '100%',
                }}
                onPress={() => {
                  sheetRef.current.snapTo(2);
                }}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

const AllReviewPage = (props) => {
  const {
    original_title: movie_title,
    id: movie_id,
    release_date: movie_release_date,
    backdrop_path: movie_picture_path,
  } = props.route.params.movieItem;
  const {
    isLoading,
    data_reviews,
    data_myreviews,
    error,
    dispatchGetReviews,
    dispatchMyReviews,
  } = props;
  const reviewDetail = useRef(null);
  const addReview = useRef(null);
  const contentScrollViewDetail = useRef(null);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [selectedReview, setSelectedReview] = useState({
    rating: '',
    headline: '',
    username: '',
    profilePicture: '',
    review: '',
    updatedAt: '',
  });
  const [canReview, setCanReview] = useState(true);

  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,
      title: movie_title,
      headerBackTitle: 'Detail',
      headerStyle: {
        backgroundColor: '#131313',
        shadowColor: 'transparent',
      },
      headerTitleStyle: {
        fontSize: scale(20),
        fontWeight: 'bold',
      },
      headerTintColor: '#FF002E',
      style: { shadowColor: 'transparent' },
    });

    const payload = {
      paramsId: movie_id,
    };

    dispatchGetReviews(payload);
    dispatchMyReviews();
  }, []);

  useEffect(() => {
    const hasReviewed = data_myreviews.find((item, i) => {
      return item.movie_id === movie_id;
    });

    if (hasReviewed) {
      setCanReview(false);
    } else {
      setCanReview(true);
    }
  }, [data_myreviews]);

  return (
    <>
      {/* <KeyboardAvoidingView behavior={'height'} style={{ flex: 1 }}> */}
      <StatusBar barStyle="light-content" />
      {canReview && (
        <TouchableOpacity
          style={styles.floatingButton}
          onPress={() => {
            setTimeout(() => {
              addReview.current.snapTo(0);
            }, 100);
          }}>
          <Icon name="plus" height={35} width={35} fill={Color.primaryColor} />
        </TouchableOpacity>
      )}

      <View style={styles.container}>
        {!isLoading && (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={data_reviews}
            keyExtractor={(item) => String(item.id)}
            ListEmptyComponent={() =>
              !isLoading && (
                <Text
                  style={{
                    justifyContent: 'center',
                    alignSelf: 'center',
                    fontSize: scale(30),
                  }}>
                  {error.message ? error.message : 'No review yet!'}
                </Text>
              )
            }
            ListFooterComponent={() =>
              // <ActivityIndicator
              //   animating={isLoading}
              //   size="large"
              //   style={{
              //     justifyContent: 'center',
              //   }}
              // />
              isLoading && (
                <View>
                  <Skeleton />
                  <Skeleton />
                  <Skeleton />
                  <Skeleton />
                  <Skeleton />
                </View>
              )
            }
            renderItem={({ item }) => (
              <CardReview
                rating={item.rating}
                headline={item.headline}
                username={item.user_details.username}
                profilePicture={item.user_details.profile_picture}
                review={item.review}
                updatedAt={item.updatedAt}
                cardStyles={styles}
                onBtnClick={() => {
                  setSelectedReview({
                    ...selectedReview,
                    rating: item.rating,
                    headline: item.headline,
                    username: item.user_details.username,
                    profilePicture: item.user_details.profile_picture,
                    review: item.review,
                    updatedAt: item.updatedAt,
                  });
                  reviewDetail.current.snapTo(0);
                }}
              />
            )}
            onEndReachedThreshold={0.2}
            onEndReached={() => {
              page < totalPages ? setPage(page + 1) : null;
            }}
          />
        )}
      </View>

      <BottomSheet
        ref={reviewDetail}
        snapPoints={['89%', 0, 0]}
        initialSnap={2}
        borderRadius={15}
        renderContent={() => (
          <View
            style={{
              backgroundColor: Color.grey,
              paddingHorizontal: scale(10),
              paddingBottom: scale(15),
              height: '100%',
            }}>
            <StripHandler />
            <ScrollView
              ref={contentScrollViewDetail}
              style={{ paddingVertical: 10 }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: MARGIN_BOTTOM,
                }}>
                {selectedReview.profilePicture ? (
                  <Image
                    source={{
                      uri: `http://139.59.124.53:3003/files/profile/thumbnail/${selectedReview.profilePicture}`,
                    }}
                    style={{
                      width: 100,
                      height: 100,
                      borderRadius: 50,
                      marginRight: 10,
                    }}
                  />
                ) : (
                  <View
                    style={{
                      width: 100,
                      height: 100,
                      borderRadius: 50,
                      marginRight: 10,
                      backgroundColor: 'gray',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Icon
                      name="person"
                      height={50}
                      width={50}
                      fill={Color.black}
                    />
                  </View>
                )}

                <View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon
                      name="star"
                      height={24}
                      width={24}
                      fill={Color.yellowStar}
                    />
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: 'bold',
                        marginRight: 10,
                      }}>
                      {selectedReview.rating ? selectedReview.rating : '-'}/10
                    </Text>
                    <Text style={{ fontSize: scale(28), fontWeight: 'bold' }}>
                      {selectedReview.headline}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: scale(20), fontWeight: 'bold' }}>
                      {selectedReview.username}
                    </Text>
                  </View>
                </View>
              </View>
              <Text
                style={{
                  fontSize: scale(18),
                  marginBottom: scale(MARGIN_BOTTOM),
                }}>
                {selectedReview.review}
              </Text>
            </ScrollView>
            <Button
              title="close"
              textStyle={{ color: Color.primaryColor, fontSize: scale(18) }}
              buttonStyle={{ backgroundColor: 'transparent' }}
              onPress={() => {
                reviewDetail.current.snapTo(2);
              }}
            />
          </View>
        )}
      />
      {/* <View style={{ height, zIndex: 0 }}> */}
      <BottomSheet
        ref={addReview}
        snapPoints={['90%', 0, 0]}
        initialSnap={2}
        borderRadius={15}
        enabledGestureInteraction={false}
        renderContent={() => (
          <SheetAddReview
            {...props}
            movie_id={movie_id}
            movie_title={movie_title}
            movie_release_date={movie_release_date}
            movie_picture_path={movie_picture_path}
            sheetRef={addReview}
          />
        )}
      />
      {/* </KeyboardAvoidingView> */}
    </>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    isLoading: state.reviewStore.isLoading,
    error: state.reviewStore.error,
    data_reviews: state.reviewStore.data_movies_reviews,
    data_myreviews: state.reviewStore.data_user_reviews,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchMyReviews: () => dispatch(getUserReviews()),
    dispatchGetReviews: (payload) => dispatch(getMovieReviews(payload)),
    dispatchSubmitReview: (payload) => dispatch(submitReview(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AllReviewPage);
