import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';

const FONT_SIZE = 18;
const BLACK = '#131313';
const GREEN = '#28A68B';
const BACKGROUND_COLOR = '#24242C';

const styles = StyleSheet.create({
  floatingButton: {
    position: 'absolute',
    width: scale(60),
    height: scale(60),
    borderRadius: scale(30),
    borderColor: GREEN,
    backgroundColor: Color.yellowStar,
    bottom: scale(30),
    right: scale(20),
    zIndex: 99,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: BLACK,
    padding: scale(10),
  },
  card: {
    marginBottom: scale(10),
    padding: scale(10),
    borderRadius: 15,
    backgroundColor: BACKGROUND_COLOR,
  },
  cardHeader: {
    flexDirection: 'row',
  },
  cardAvatar: {
    marginRight: scale(10),
  },
  reviewAvatar: {
    height: 70,
    width: 70,
    borderRadius: 35,
    marginRight: 10,
  },
  cardHeaderContent: {
    justifyContent: 'center',
  },
  cardHeaderScore: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  score: {
    fontSize: FONT_SIZE,
    marginLeft: scale(5),
    marginRight: scale(10),
  },
  cardHeaderReviewer: {
    flexDirection: 'row',
  },
  reviewer: {
    fontWeight: 'bold',
    fontSize: FONT_SIZE,
  },
  content: {
    flex: 1,
    fontSize: FONT_SIZE,
    padding: 5,
  },
  defaultText: {
    fontSize: FONT_SIZE,
  },
});

export default styles;
