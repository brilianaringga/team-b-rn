/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { View } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { Color, scale } from 'utils';

const Skeleton = () => {
  return (
    <View
      style={{
        backgroundColor: Color.grey,
        padding: scale(10),
        borderRadius: scale(15),
        marginBottom: scale(10),
      }}>
      <SkeletonPlaceholder>
        <View style={{ flexDirection: 'row', marginBottom: scale(10) }}>
          <View
            style={{
              height: 70,
              width: 70,
              borderRadius: 35,
              marginRight: 10,
            }}
          />
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-evenly',
            }}>
            <View
              style={{
                width: '100%',
                height: scale(16),
                borderRadius: scale(100),
              }}
            />
            <View
              style={{
                width: '100%',
                height: scale(16),
                borderRadius: scale(100),
              }}
            />
          </View>
        </View>

        <View
          style={{
            width: '100%',
            height: scale(16),
            borderRadius: scale(100),
            marginBottom: scale(10),
          }}
        />

        <View
          style={{
            width: '100%',
            height: scale(16),
            borderRadius: scale(100),
            marginBottom: scale(10),
          }}
        />

        <View
          style={{
            width: '100%',
            height: scale(42),
            borderRadius: scale(10),
            marginBottom: scale(10),
          }}
        />
      </SkeletonPlaceholder>
    </View>
  );
};

export default Skeleton;
