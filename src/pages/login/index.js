import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  ImageBackground,
  Image,
  StyleSheet,
  useNavigation,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  AsyncStorage,
  TextInput,
  TouchableOpacity,
  StatusBar,
} from 'libraries';
import { Text, Button } from 'components';
import styles from './style';
import { appString, scale, Color } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import { HelperText } from 'react-native-paper'
import { LoginAction } from 'configs/redux/reducers/auth/authActions';
import LottieView from 'lottie-react-native';
import {Form,Input, Item, Label} from 'native-base'
import { navigate } from 'configs/routes/RootNavigation';
import * as RootNavigation from 'configs/routes/RootNavigation';

const LoginPage = (props) => {
  const {
    dispatchLogin,
    navigation,
    dispatchExample,
    contoh_email,
    contoh_data,
    dispatchGetApi,
    contoh_data_api,
    email,
    error,
    isLoading,
  } = props;

  const [MyUsername,setMyUsername]=useState(null);
  const [MyPassword,setMyPassword]=useState(null);
  const [Error,setMyError]=useState(null);
  const [OnError,setOnError]=useState(false);
  const [IsLoading, setIsLodaing]=useState(isLoading);
  
  useEffect(() => {
    // cekToken ()
    // isi initial first load
    //contoh update state global via redux
    dispatchExample();
    const start = 1;
    const limit = 2;
    dispatchGetApi(start, limit);
  }, []);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    // console.log('ini data redux')
    // console.log(email);
    console.log(error);
    setOnError(error ? true:false)
    setMyError(error)
  }, [error]);


  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    // console.log('ini data redux')
    // console.log(email);
    // if (email) {
    //   navigation.reset({
    //     index:0,
    //     routes: [{name:'ProfilPage'}]})
    //}
    
  }, []);


  doLogin = () => {
    const payload = {
        body: {
          username: MyUsername,
          password: MyPassword,
        },
      }

      dispatchLogin(payload)
      

    // let token = await AsyncStorage.getItem('@token')
    // console.log('token')
    // console.log(token)
    //await this.props.dispatchExample(data);
  };

  // cekToken = async () => {
  //   try{
  //     const token = await AsyncStorage.getItem('@token')
  //     console.log('test')
  //     console.log(token)
  //     if (token != null) {
  //       console.log('ada')
  //       RootNavigation.reset('BottomTab')
  //     }
  //   }
  //   catch (error){

  //   }
  // }



  const styles = StyleSheet.create({
    TextStyle: {
      fontWeight: 'bold',
      fontSize: 70,
      color: '#FF002E',
      marginTop: 10,
      marginBottom: 20,
    },
  });

  // const doLogin = () => {
  //   const payload = {
  //     body: {
  //       username: 'indracip',
  //       password: 'telkomsel',
  //     },
  //   };
  //   console.log('masuk');
  //   dispatchlogin(payload);
  // };

  return (
    <ImageBackground
      style={{ flex: 1 }}
      source={require('../../assets/images/background.jpg')}>
      <SafeAreaView style={{ flex: 1 }}>
      <StatusBar backgroundColor={Color.black} barStyle={'light-content'} />
        <KeyboardAvoidingView
          keyboardVerticalOffset={-230}
          behavior='padding'
          style={{flex:1}}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
    
  
        <View style={{flex:1, justifyContent:'center', marginHorizontal: scale (30) }}>
  
        <LottieView style ={{marginBottom: scale (180)}}source={require('../../assets/animasi/movie1.json')} 
          autoPlay loop
          ></LottieView>
        <View >
        <Form style={{borderColor:'white'}}>
            <Item  style={{borderColor:'white'}} floatingLabel>
              <Label style={{color:'white'}} >Username</Label>
              <Input style={{color:'white'}} placeholderTextColor='white' autoCapitalize='none'
              onChangeText={(username) => setMyUsername(username)}
              value={MyUsername}
              />
            </Item>
            <Item style={{borderColor:'white'}} floatingLabel>
              <Label style={{color:'white'}}>Password</Label>
              <Input style={{color:'white'}} placeholderTextColor='white'autoCapitalize='none' secureTextEntry={true}
              onChangeText={(password) => setMyPassword(password)}
              value={MyPassword}
              />
            </Item>
          </Form>
          {/* <TextInput 
            
            textAlign='center'
            placeholder='Email'
            // theme={{roundness:50,colors:{primary:'#FF002E'}}}
            // label = "UserName"
            // mode = "outlined"
            onChangeText={(username) => setMyUsername(username)}
            value={MyUsername}></TextInput>
          <TextInput 
            
            textAlign='center'
            placeholder='Password'
            // theme={{roundness:50,colors:{primary:'#FF002E'}}}
            // style={{marginTop:10}}
            // secureTextEntry={true}
            // label = "Password"
            // mode = "outlined"
            onChangeText={(password) => setMyPassword(password)}
            value={MyPassword}></TextInput> */}
         </View>


         <View style={{alignItems:'center'}}>

         <HelperText type="error" visible={OnError}>
            {Error && Error.message ?
            Error.message:'Something Went Wrong'}
          </HelperText>

            {/* <Text style={{marginTop:10,color:'white'}}>Forgot your password?</Text> */}

            <Button title={'SIGN IN'} onPress={()=> doLogin()}></Button>
            {/* <Button mode='contained'
              style={{marginTop:10,}}
              theme={{roundness:50,colors:{primary:'#FF002E'}}}
              onPress={()=> doLogin()}>
               SIGN IN
             </Button> */}
        
             
            <Text style={{marginTop: scale(10),color:'white'}}>Don't have an account?
              <Text onPress={()=>{navigation.navigate('RegisterPage')}} style={{color:'white'}}>  Sign UP</Text>
            </Text>
           

            {/* <Button mode='outlined'
              theme={{roundness:50,colors:{primary:'#FF002E'}}}
              style={{marginTop:10,}}
              onPress={()=> navigation.navigate('RegisterPage')}>
              SIGN UP
            </Button> */}

        </View>
        </View>
      
      
      </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
      </SafeAreaView>
    </ImageBackground>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    contoh_data: state.exampleStore.contoh_data,
    contoh_email: state.exampleStore.contoh_email,
    contoh_data_api: state.exampleStore.contoh_data_api,

    email: state.authStore.email,
    error: state.authStore.error,
    isLoading: state.authStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchLogin: (payload) => dispatch(LoginAction(payload)),

    dispatchExample: (data) => dispatch(exampleAction(data)),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
