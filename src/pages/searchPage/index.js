import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  StatusBar,
  TextInput,
  ScrollView,
  FlatGrid,
  TouchableOpacity,
  Image,
  moment,
  Pressable,
  ActivityIndicator,
  FlatList,
  Keyboard,
} from 'libraries';
import { Icon } from 'react-native-eva-icons';

import { Text } from 'components';
import styles from './style';
import { appString, Color, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import {
  getMovieListbyGenre,
  searchMovies,
} from 'configs/redux/reducers/movies/moviesActions';

const SearchPage = (props) => {
  const {
    navigation,
    dispatchExample,
    contoh_email,
    contoh_data,
    dispatchGetApi,
    contoh_data_api,
    data_genre,
    dispatchGetMovieByGenre,
    data_movie_genre,
    dispatchSearchMovies,
    data_search,
    error,
    isLoadingSearch,
  } = props;

  const [items, setItems] = React.useState([
    { name: 'TURQUOISE', code: '#1abc9c' },
    { name: 'EMERALD', code: '#2ecc71' },
    { name: 'PETER RIVER', code: '#3498db' },
    { name: 'AMETHYST', code: '#9b59b6' },
    { name: 'WET ASPHALT', code: '#34495e' },
    { name: 'GREEN SEA', code: '#16a085' },
    { name: 'NEPHRITIS', code: '#27ae60' },
    { name: 'BELIZE HOLE', code: '#2980b9' },
    { name: 'WISTERIA', code: '#8e44ad' },
    { name: 'MIDNIGHT BLUE', code: '#2c3e50' },
    { name: 'SUN FLOWER', code: '#f1c40f' },
    { name: 'CARROT', code: '#e67e22' },
    { name: 'ALIZARIN', code: '#e74c3c' },
    { name: 'CLOUDS', code: '#ecf0f1' },
    { name: 'CONCRETE', code: '#95a5a6' },
    { name: 'ORANGE', code: '#f39c12' },
    { name: 'PUMPKIN', code: '#d35400' },
    { name: 'POMEGRANATE', code: '#c0392b' },
    { name: 'SILVER', code: '#bdc3c7' },
    { name: 'ASBESTOS', code: '#7f8c8d' },
  ]);

  const MovieData = [
    {
      adult: false,
      backdrop_path: '/zO1nXPpmJylWVHg2eL00HysZqE5.jpg',
      genre_ids: [28, 16, 878, 10751],
      id: 13640,
      original_language: 'en',
      original_title: 'Superman: Doomsday',
      overview:
        'When LexCorps accidentally unleash a murderous creature, Doomsday, Superman meets his greatest challenge as a champion. Based on the "The Death of Superman" storyline that appeared in DC Comics\' publications in the 1990s',
      poster_path: '/itvuWm7DFWWzWgW0xgiaKzzWszP.jpg',
      release_date: '2007-09-18',
      title: 'Superman: Doomsday',
      video: false,
      vote_average: 6.5,
      vote_count: 334,
      popularity: 76.66,
    },
    {
      adult: false,
      backdrop_path: '/AjtPveq4ZdDDvMGHY4Vi4LIM8oe.jpg',
      genre_ids: [28, 16],
      id: 379291,
      original_language: 'en',
      original_title: 'Justice League vs. Teen Titans',
      overview:
        'Robin is sent by Batman to work with the Teen Titans after his volatile behavior botches up a Justice League mission. The Titans must then step up to face Trigon after he possesses the League and threatens to conquer the world.',
      poster_path: '/3G6RPpafXApTzn56cbVqLBp9uSz.jpg',
      release_date: '2016-03-29',
      title: 'Justice League vs. Teen Titans',
      video: false,
      vote_average: 7.2,
      vote_count: 616,
      popularity: 47.06,
    },
    {
      backdrop_path: '/2ex2beZ4ssMeOduLD0ILzXKCiep.jpg',
      genre_ids: [28, 12, 878, 14],
      original_language: 'en',
      original_title: 'X-Men: Apocalypse',
      poster_path: '/2mtQwJKVKQrZgTz49Dizb25eOQQ.jpg',
      title: 'X-Men: Apocalypse',
      vote_average: 6.5,
      vote_count: 10115,
      overview:
        "After the re-emergence of the world's first mutant, world-destroyer Apocalypse, the X-Men must unite to defeat his extinction level plan.",
      release_date: '2016-05-18',
      video: false,
      id: 246655,
      adult: false,
      popularity: 62.633,
    },
    {
      genre_ids: [28, 12, 878],
      original_language: 'en',
      original_title: 'Captain Marvel',
      id: 299537,
      video: false,
      vote_average: 7.0,
      overview:
        'The story follows Carol Danvers as she becomes one of the universe’s most powerful heroes when Earth is caught in the middle of a galactic war between two alien races. Set in the 1990s, Captain Marvel is an all-new adventure from a previously unseen period in the history of the Marvel Cinematic Universe.',
      release_date: '2019-03-06',
      vote_count: 10687,
      title: 'Captain Marvel',
      adult: false,
      backdrop_path: '/w2PMyoyLU22YvrGK3smVM9fW1jj.jpg',
      poster_path: '/AtsgWhDnHTq68L0lLsUrCnM7TjG.jpg',
      popularity: 249.603,
    },
    {
      original_language: 'en',
      original_title: 'Iron Man 2',
      poster_path: '/6WBeq4fCfn7AN0o21W9qNcRF2l9.jpg',
      video: false,
      vote_average: 6.8,
      overview:
        "With the world now aware of his dual life as the armored superhero Iron Man, billionaire inventor Tony Stark faces pressure from the government, the press and the public to share his technology with the military. Unwilling to let go of his invention, Stark, with Pepper Potts and James 'Rhodey' Rhodes at his side, must forge new alliances – and confront powerful enemies.",
      release_date: '2010-04-28',
      vote_count: 15489,
      title: 'Iron Man 2',
      adult: false,
      backdrop_path: '/jesRqfL9v6HNnowe795xjmuKUXl.jpg',
      id: 10138,
      genre_ids: [12, 28, 878],
      popularity: 65.555,
    },
    {
      original_language: 'en',
      original_title: 'Iron Man',
      poster_path: '/78lPtwv72eTNqFW9COBYI0dWDJa.jpg',
      video: false,
      vote_average: 7.6,
      overview:
        'After being held captive in an Afghan cave, billionaire engineer Tony Stark creates a unique weaponized suit of armor to fight evil.',
      release_date: '2008-04-30',
      vote_count: 19783,
      title: 'Iron Man',
      adult: false,
      backdrop_path: '/vbY95t58MDArtyUXUIb8Fx1dCry.jpg',
      id: 1726,
      genre_ids: [28, 878, 12],
      popularity: 77.105,
    },
    {
      adult: false,
      backdrop_path: '/yFuKvT4Vm3sKHdFY4eG6I4ldAnn.jpg',
      genre_ids: [28, 12, 878],
      vote_count: 16104,
      original_language: 'en',
      original_title: 'Captain America: The First Avenger',
      poster_path: '/vSNxAJTlD0r02V9sPYpOjqDZXUK.jpg',
      title: 'Captain America: The First Avenger',
      video: false,
      vote_average: 6.9,
      release_date: '2011-07-22',
      overview:
        "During World War II, Steve Rogers is a sickly man from Brooklyn who's transformed into super-soldier Captain America to aid in the war effort. Rogers must stop the Red Skull – Adolf Hitler's ruthless head of weaponry, and the leader of an organization that intends to use a mysterious device of untold powers for world domination.",
      id: 1771,
      popularity: 44.13,
    },
    {
      genre_ids: [28, 12, 14, 878],
      original_language: 'en',
      original_title: 'Fantastic Four',
      id: 9738,
      video: false,
      vote_average: 5.8,
      overview:
        'During a space voyage, four scientists are altered by cosmic rays: Reed Richards gains the ability to stretch his body; Sue Storm can become invisible; Johnny Storm controls fire; and Ben Grimm is turned into a super-strong … thing. Together, these "Fantastic Four" must now thwart the evil plans of Dr. Doom and save the world from certain destruction.',
      release_date: '2005-06-29',
      vote_count: 7177,
      title: 'Fantastic Four',
      adult: false,
      backdrop_path: '/isDbx0OEFgYKevzbg7wBJZmQgbM.jpg',
      poster_path: '/8HLQLILZLhDQWO6JDpvY6XJLH75.jpg',
      popularity: 24.394,
    },
    {
      adult: false,
      backdrop_path: '/wPgcxHNvnEEMXx94l2911gqZ4o9.jpg',
      genre_ids: [28, 12, 878],
      original_language: 'en',
      original_title: 'Iron Man 3',
      poster_path: '/qhPtAc1TKbMPqNvcdXSOn9Bn7hZ.jpg',
      video: false,
      id: 68721,
      vote_count: 17149,
      overview:
        "When Tony Stark's world is torn apart by a formidable terrorist called the Mandarin, he starts an odyssey of rebuilding and retribution.",
      release_date: '2013-04-18',
      vote_average: 6.9,
      title: 'Iron Man 3',
      popularity: 64.583,
    },
    {
      genre_ids: [28, 16, 12, 878],
      original_language: 'en',
      original_title: 'Ultimate Avengers: The Movie',
      poster_path: '/iMCkGHVrYRdqKROPRPmVaJVSlg3.jpg',
      title: 'Ultimate Avengers: The Movie',
      id: 14609,
      overview:
        'When a nuclear missile was fired at Washington in 1945, Captain America managed to detonate it in the upper atmosphere. But then he fell miles into the icy depths of the North Atlantic, where he remained lost for over sixty years. But now, with the world facing the very same evil, Captain America must rise again as our last hope for survival.',
      release_date: '2006-02-21',
      vote_count: 233,
      vote_average: 6.7,
      adult: false,
      backdrop_path: '/9tjIgkkbajG2zMI4Yk21hpttXv0.jpg',
      video: false,
      popularity: 31.902,
    },
    {
      genre_ids: [28, 12, 878, 14, 9648],
      original_language: 'en',
      original_title: 'Captain Marvel 2',
      poster_path: '/gWxeVJ920nprJEbXbzmuuzeaAar.jpg',
      title: 'Captain Marvel 2',
      id: 609681,
      overview: 'The sequel to Captain Marvel (2019).',
      release_date: '2022-11-10',
      vote_count: 0,
      vote_average: 0.0,
      adult: false,
      backdrop_path: '/iGtSJbDku0riOw1joO1qD4pGwdm.jpg',
      video: false,
      popularity: 37.806,
    },
    {
      overview:
        'As Batman hunts for the escaped Joker, the Clown Prince of Crime attacks the Gordon family to prove a diabolical point mirroring his own fall into madness.',
      id: 382322,
      adult: false,
      backdrop_path: '/wNyhtxSazhYVSG1So8zgGTqVFVo.jpg',
      vote_count: 1231,
      genre_ids: [28, 16, 80, 18],
      release_date: '2016-07-24',
      original_language: 'en',
      original_title: 'Batman: The Killing Joke',
      poster_path: '/nxncAAL1FUKtQWs4uhs5jf1MVut.jpg',
      title: 'Batman: The Killing Joke',
      video: false,
      vote_average: 6.6,
      popularity: 30.271,
    },
    {
      adult: false,
      backdrop_path: '/mUc1RPyhk2LR7FHkJ0yslvXMWu6.jpg',
      genre_ids: [28, 12, 16],
      id: 30061,
      original_language: 'en',
      original_title: 'Justice League: Crisis on Two Earths',
      overview:
        "A heroic version of Lex Luthor from an alternate universe appears to recruit the Justice League to help save his Earth from the Crime Syndicate, an evil version of the League. What ensues is the ultimate battle of good versus evil in a war that threatens both planets and, through a devious plan launched by Batman's counterpart Owlman, puts the balance of all existence in peril.",
      poster_path: '/5Cjrxbv0BIxzgKFC9PYcgmPtnMV.jpg',
      release_date: '2010-02-23',
      title: 'Justice League: Crisis on Two Earths',
      video: false,
      vote_average: 7.3,
      vote_count: 416,
      popularity: 20.593,
    },
    {
      adult: false,
      backdrop_path: '/qkdmZFwy8wTGEHgPAS1HX2lOdK6.jpg',
      genre_ids: [28, 12, 16, 878, 10751],
      id: 14011,
      original_language: 'en',
      original_title: 'Justice League: The New Frontier',
      overview:
        'The human race is threatened by a powerful creature, and only the combined power of Superman, Batman, Wonder Woman, Green Lantern, Martian Manhunter and The Flash can stop it. But can they overcome their differences to thwart this enemy using the combined strength of their newly formed Justice League?',
      poster_path: '/bj25ueeYNkz4e5t8oMMaAOWmwOc.jpg',
      release_date: '2008-02-26',
      title: 'Justice League: The New Frontier',
      video: false,
      vote_average: 6.8,
      vote_count: 315,
      popularity: 64.819,
    },
    {
      backdrop_path: '/bEiIOflSHB0GGyiPf7dX1XmvlMU.jpg',
      genre_ids: [28, 16, 9648],
      original_language: 'en',
      original_title: 'Batman: Under the Red Hood',
      poster_path: '/dEujs48u3n4cmh15ITtHHDFxSaS.jpg',
      title: 'Batman: Under the Red Hood',
      vote_average: 7.8,
      vote_count: 1024,
      overview:
        "There's a mystery afoot in Gotham City, and Batman must go toe-to-toe with a mysterious vigilante, who goes by the name of Red Hood. Subsequently, old wounds reopen and old, once buried memories come into the light.  Batman faces his ultimate challenge as the mysterious Red Hood takes Gotham City by firestorm. One part vigilante, one part criminal kingpin, Red Hood begins cleaning up Gotham with the efficiency of Batman, but without following the same ethical code.",
      release_date: '2010-07-27',
      video: false,
      id: 40662,
      adult: false,
      popularity: 30.669,
    },
    {
      adult: false,
      backdrop_path: '/e7jIX02GiSwsgkU5lMpeKjwq2Zc.jpg',
      genre_ids: [14, 28, 80, 878, 53],
      id: 9480,
      original_language: 'en',
      original_title: 'Daredevil',
      overview:
        'He dwells in a world of eternal night, but the blackness is filled with sounds and scents, tastes and textures that most cannot perceive. Although attorney Matt Murdock is blind, his other four senses function with superhuman sharpness. By day, Murdock represents the downtrodden. At night he is Daredevil, a masked vigilante stalking the dark streets of the city, a relentless avenger of justice.',
      poster_path: '/oCDBwSkntYamuw8VJIxMRCtDBmi.jpg',
      release_date: '2003-02-14',
      title: 'Daredevil',
      video: false,
      vote_average: 5.2,
      vote_count: 3656,
      popularity: 28.734,
    },
    {
      overview:
        'An adaptation of Mark Waid\'s "Tower of Babel" story from the JLA comic. Vandal Savage steals confidential files Batman has compiled on the members of the Justice League, and learns all their weaknesses.',
      release_date: '2012-02-28',
      adult: false,
      backdrop_path: '/50gUUeIQZXHI1vXtuyiFAYa499u.jpg',
      title: 'Justice League: Doom',
      genre_ids: [28, 16],
      original_language: 'en',
      original_title: 'Justice League: Doom',
      poster_path: '/seCbcjdZYUl8SRKjeWfyi2ngzFj.jpg',
      vote_count: 533,
      video: false,
      id: 76589,
      vote_average: 7.4,
      popularity: 78.222,
    },
    {
      genre_ids: [878, 16, 28],
      original_language: 'en',
      original_title: 'Teen Titans: The Judas Contract',
      poster_path: '/4MumpGZmbTvHFzxyJ9MtSpqD4FH.jpg',
      video: false,
      vote_average: 7.5,
      vote_count: 420,
      overview:
        "Tara Markov is a girl who has power over earth and stone; she is also more than she seems. Is the newest Teen Titan an ally or a threat? And what are the mercenary Deathstroke's plans for the Titans?",
      release_date: '2017-03-31',
      title: 'Teen Titans: The Judas Contract',
      id: 408647,
      adult: false,
      backdrop_path: '/cL2myZ8118HWjbLUCCaxJ9IdZ6d.jpg',
      popularity: 31.57,
    },
    {
      original_title: 'Batman: Bad Blood',
      poster_path: '/1UmPJWfaivtNjsScJqcsbsQRGNY.jpg',
      video: false,
      vote_average: 7.1,
      overview:
        "Bruce Wayne is missing. Alfred covers for him while Nightwing and Robin patrol Gotham City in his stead and a new player, Batwoman, investigates Batman's disappearance.",
      release_date: '2016-03-22',
      vote_count: 506,
      adult: false,
      backdrop_path: '/jLslJietfQJEgOvQHk4fRYFCnTS.jpg',
      title: 'Batman: Bad Blood',
      genre_ids: [28, 16, 878],
      id: 366924,
      original_language: 'en',
      popularity: 24.015,
    },
    {
      original_language: 'en',
      original_title: 'Batman Begins',
      poster_path: '/1P3ZyEq02wcTMd3iE4ebtLvncvH.jpg',
      video: false,
      vote_average: 7.7,
      overview:
        'Driven by tragedy, billionaire Bruce Wayne dedicates his life to uncovering and defeating the corruption that plagues his home, Gotham City.  Unable to work within the system, he instead creates a new identity, a symbol of fear for the criminal underworld - The Batman.',
      release_date: '2005-06-10',
      vote_count: 15564,
      title: 'Batman Begins',
      adult: false,
      backdrop_path: '/lh5lbisD4oDbEKgUxoRaZU8HVrk.jpg',
      id: 272,
      genre_ids: [28, 80, 18],
      popularity: 56.448,
    },
  ];

  const [curState, setCurState] = useState({
    selectedId: 28,
    viewMode: 'genre',
    dataList: [],
    offset: 1,
    offsetSearch: 1,
    dataSearch: [],
  });
  const [query, setQuery] = useState('');
  const [notFound, setNotFound] = useState(false);

  let onEndReachedCalledDuringMomentum = true;

  useEffect(() => {
    // isi initial first load
    //contoh update state global via redux
    // dispatchExample();
    // const start = 1;
    // const limit = 2;
    // dispatchGetApi(start, limit);
    // setSelectedId(data_genre[0].id);
    setCurState({
      ...curState,
      selectedId: 28,
    });
    const payload = {
      params: {
        page: 1,
        genre_id: 28,
      },
    };
    dispatchGetMovieByGenre(payload);
  }, []);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    console.log('berubah');
    console.log(curState.dataList);
    // setDataList([...curState.dataList, ...data_movie_genre]);
    setCurState({
      ...curState,
      dataList: [...curState.dataList, ...data_movie_genre],
    });
  }, [data_movie_genre]);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah

    // setDataList([...curState.dataList, ...data_movie_genre]);
    setCurState({
      ...curState,
      dataSearch: [...curState.dataSearch, ...data_search],
    });
  }, [data_search]);

  useEffect(() => {
    if (error.search.status == 404) {
      setCurState({
        ...curState,
        dataSearch: [],
      });
      setNotFound(true);
    }
  }, [error]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={Color.black} barStyle={'light-content'} />
      <Text
        bold
        style={{
          color: 'white',
          fontSize: scale(30),
          marginBottom: scale(10),
        }}>
        {'Find your Favorite Movies \nor Genre ...'}
      </Text>
      <TextInput
        placeholder={'Search Movies'}
        placeholderTextColor={'#D2D4D4'}
        style={{
          backgroundColor: '#24242C',
          paddingHorizontal: scale(15),
          borderRadius: scale(50),
          marginBottom: scale(10),
          color: 'white',
        }}
        onChangeText={(text) => {
          if (text === '') {
            Keyboard.dismiss;
            console.log('back to genre mode');
            setNotFound(false);
            setCurState({
              ...curState,
              viewMode: 'genre',
              dataSearch: [],
            });
            // const payload = {
            //   params: {
            //     page: 1,
            //     genre_id: curState.selectedId,
            //   },
            // };
            // dispatchGetMovieByGenre(payload);
          }
          setQuery(text);
        }}
        onSubmitEditing={() => {
          if (query != '') {
            console.log('seacrh mode');
            setNotFound(false);
            setCurState({
              ...curState,
              offsetSearch: 1,
              viewMode: 'search',
            });
            const payload = {
              params: {
                title: query,
              },
            };
            dispatchSearchMovies(payload);
          }
          Keyboard.dismiss;
        }}
        returnKeyType="search"
        value={query}
      />
      {curState.viewMode == 'genre' ? (
        <View>
          <ScrollView horizontal>
            {data_genre.map((item, index) => (
              <Pressable
                key={index}
                onPress={() => {
                  // setSelectedId(item.id);
                  setCurState({
                    ...curState,
                    selectedId: item.id,
                    viewMode: 'genre',
                    offset: 1,
                    dataList: [],
                  });
                  // setViewMode('genre');
                  // setOffset(1);
                  // setDataList([]);
                  const payload = {
                    params: {
                      page: 1,
                      genre_id: item.id,
                    },
                  };
                  dispatchGetMovieByGenre(payload);
                }}>
                <View style={{ marginRight: scale(15) }}>
                  <Text
                    medium={curState.selectedId == item.id ? true : false}
                    style={{
                      color:
                        curState.selectedId == item.id
                          ? Color.primaryColor
                          : 'white',
                      fontSize: scale(17),
                    }}>
                    {item.name}
                  </Text>
                  {curState.selectedId == item.id ? (
                    <View
                      style={{
                        height: scale(1.3),
                        width: scale(item.name.length * 3),
                        backgroundColor: Color.primaryColor,
                        borderRadius: scale(15),
                      }}></View>
                  ) : (
                    <View></View>
                  )}
                </View>
              </Pressable>
            ))}
          </ScrollView>
        </View>
      ) : (
        <View>
          <Text
            style={{
              color: 'white',
              fontSize: scale(20),
              marginBottom: scale(10),
            }}>
            Search results from: {query}
          </Text>
        </View>
      )}
      {curState.viewMode == 'genre' ? (
        <View
          style={{
            flex: 1,
            marginHorizontal: scale(-10),
          }}>
          <FlatGrid
            itemDimension={scale(110)}
            data={curState.dataList}
            style={{ marginTop: 10, flex: 1 }}
            // staticDimension={300}
            // fixed

            spacing={5}
            renderItem={({ item }) => (
              <Pressable
                onPress={() => {
                  navigation.navigate('MovieDetailsPage', {
                    movieId: item.id,
                    holdRender: true,
                  });
                }}>
                <View
                  style={[
                    {
                      justifyContent: 'flex-end',
                      borderRadius: 5,
                      // padding: 10,
                      height: scale(150),
                      position: 'relative',
                    },
                    // { backgroundColor: item.code },
                  ]}>
                  <Image
                    style={{
                      height: scale(150),
                      width: scale(110),
                      borderRadius: scale(7),
                    }}
                    source={{
                      uri: `https://image.tmdb.org/t/p/w500${item.poster_path}`,
                    }}
                  />
                  <View
                    style={{
                      width: scale(110),
                      position: 'absolute',
                      bottom: 0,
                      backgroundColor: 'rgba(0,0,0,0.35)',
                      minHeight: scale(20),
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      medium
                      style={{
                        fontSize: scale(15),
                        color: 'white',
                        textShadowColor: 'rgba(0, 0, 0, 0.85)',
                        textShadowOffset: { width: 1, height: 1 },
                        textShadowRadius: 20,
                        textAlign: 'center',
                      }}>
                      {item.title} (
                      {moment(new Date(item.release_date)).format('Y')})
                    </Text>
                  </View>
                  {/* <Text style={{ fontSize: 16, color: '#fff', fontWeight: '600' }}>
                {item.name}
              </Text>
              <Text style={{ fontWeight: '600', fontSize: 12, color: '#fff' }}>
                {item.code}
              </Text> */}
                </View>
              </Pressable>
            )}
            onMomentumScrollBegin={() => {
              onEndReachedCalledDuringMomentum = false;
            }}
            onEndReached={() => {
              if (!onEndReachedCalledDuringMomentum) {
                setCurState({
                  ...curState,
                  offset: curState.offset + 1,
                });
                // setOffset(curState.offset + 1);
                const payload = {
                  params: {
                    page: curState.offset + 1,
                    genre_id: curState.selectedId,
                  },
                };
                dispatchGetMovieByGenre(payload);
                onEndReachedCalledDuringMomentum = true;
              }
            }}
            onEndReachedThreshold={0.2}
            ListFooterComponent={() => {
              return (
                <ActivityIndicator size="small" color={Color.primaryColor} />
              );
            }}
          />
        </View>
      ) : isLoadingSearch ? (
        <ActivityIndicator size="small" color={Color.primaryColor} />
      ) : !notFound ? (
        <View style={{ flex: 1 }}>
          <FlatList
            data={curState.dataSearch}
            renderItem={({ item }) => {
              console.log(item.title);
              return (
                <Pressable
                  onPress={() => {
                    navigation.navigate('MovieDetailsPage', {
                      movieId: item.id,
                      holdRender: true,
                    });
                  }}>
                  <View
                    style={{
                      backgroundColor: '#24242C',
                      borderRadius: scale(10),
                      marginBottom: scale(10),
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ position: 'relative' }}>
                        <Image
                          source={{
                            uri: `https://image.tmdb.org/t/p/w300${item.backdrop_path}`,
                          }}
                          style={{
                            height: scale(80),
                            width: scale(120),
                            borderRadius: scale(5),
                          }}
                        />
                        <View
                          style={{
                            position: 'absolute',
                            top: scale(1),
                            right: scale(1),
                            backgroundColor: 'rgba(0,0,0,0.35)',
                            paddingHorizontal: scale(2),
                            borderRadius: scale(5),
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                            }}>
                            <Icon
                              name={'star'}
                              height={scale(10)}
                              width={scale(10)}
                              fill={Color.yellowStar}
                            />
                            <Text bold style={{ color: 'white' }}>
                              {item.vote_average}
                            </Text>
                          </View>
                        </View>
                      </View>

                      <View style={{ flex: 1, marginLeft: scale(8) }}>
                        <Text
                          numberOfLines={1}
                          style={{ color: 'white', fontSize: scale(20) }}>
                          {item.title}
                        </Text>

                        <Text bold style={{ color: '#D2D4D4' }}>
                          {moment(new Date(item.release_date)).format('LL')}
                        </Text>
                        <Text numberOfLines={2} style={{ color: '#D2D4D4' }}>
                          {item.overview}
                        </Text>
                      </View>
                    </View>
                  </View>
                </Pressable>
              );
            }}
            keyExtractor={(item, index) => index.toString()}
            // ListFooterComponent={() => {
            //   return (
            //     <ActivityIndicator size="small" color={Color.primaryColor} />
            //   );
            // }}
          />
        </View>
      ) : (
        <Text style={{ color: 'white' }}>Gak ketemu</Text>
      )}
      <View style={{ height: scale(50) }}></View>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    contoh_data: state.exampleStore.contoh_data,
    contoh_email: state.exampleStore.contoh_email,
    contoh_data_api: state.exampleStore.contoh_data_api,
    data_genre: state.movieStore.data_genre,
    data_movie_genre: state.movieStore.data_movie_genre,
    data_search: state.movieStore.data_search,
    error: state.movieStore.error,
    isLoadingSearch: state.movieStore.isLoadingSearch,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchExample: () => dispatch(exampleAction()),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
    dispatchGetMovieByGenre: (payload) =>
      dispatch(getMovieListbyGenre(payload)),
    dispatchSearchMovies: (payload) => dispatch(searchMovies(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
