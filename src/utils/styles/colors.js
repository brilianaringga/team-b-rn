/**
 * @name Colors
 */

export const Color = {
  primaryColor: '#FF002E',
  black: '#131313',
  yellowStar: '#F0CE32',
  grey: '#24242C',
  green: '#28A68B',
  white: 'white'
};
