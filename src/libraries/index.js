import React from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {
  BottomTabBar,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import moment from 'moment';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import * as indicator from 'react-native-indicator';
import { WebView } from 'react-native-webview';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import Stars from 'react-native-stars';
import Video from 'react-native-video';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import changeNavigationBarColor, {
  hideNavigationBar,
  showNavigationBar,
} from 'react-native-navigation-bar-color';
import { FlatGrid, SectionGrid } from 'react-native-super-grid';
import { showMessage, hideMessage } from 'react-native-flash-message';
import ReadMore from '@fawazahmed/react-native-read-more';
import YoutubePlayer from 'react-native-youtube-iframe';
import Share from 'react-native-share';

export {
  React,
  FontAwesome5,
  AsyncStorage,
  axios,
  _,
  PropTypes,
  createBottomTabNavigator,
  moment,
  SkeletonPlaceholder,
  indicator,
  WebView,
  FastImage,
  BottomTabBar,
  connect,
  Animated,
  BottomSheet,
  Stars,
  Video,
  Carousel,
  Pagination,
  changeNavigationBarColor,
  hideNavigationBar,
  showNavigationBar,
  FlatGrid,
  SectionGrid,
  showMessage,
  hideMessage,
  ReadMore,
  YoutubePlayer,
  Share,
};

export * from 'react';
export * from 'react-native';
export * from '@react-navigation/native';
