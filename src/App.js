/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Routes } from 'configs';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import store from 'configs/redux/store';
import FlashMessage from 'react-native-flash-message';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <SafeAreaProvider>
          <Routes />
          <FlashMessage position="top" autoHide={true} />
        </SafeAreaProvider>
      </Provider>
    );
  }
}

export default App;
